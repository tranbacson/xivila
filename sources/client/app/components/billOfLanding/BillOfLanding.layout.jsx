import React from 'react';
import values from 'lodash/values';
import forEach from 'lodash/forEach';
import Table from 'rc-table';
import {labels} from './_data';
import Tools from 'helpers/Tools';
import NavWrapper from 'utils/components/NavWrapper';
import CustomModal from 'utils/components/CustomModal';
import MainTable from './tables/Main.table';
import MainForm from './forms/Main.form';
import StatusFilter from './components/statusFilter/StatusFilter';
import WaitingMessage from 'utils/components/WaitingMessage';
import Select from 'react-select';
import {TableFilter} from 'utils/components/table/TableComponents';


class BillOfLandingLayout extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			landingStatusFilter: 'all'
		};
		this._renderFilter = this._renderFilter.bind(this)
	}

	_renderFilter(){
		return (
			<div className="row">
				<div className="col-md-8">
					<TableFilter
						onFilter={this.props.onFilter}
						onRemove={this.props.onRemove}
						bulkRemove={false}
						/>
				</div>
				<div className="col-md-4">
					<Select
						name="landing_status_filter"
						valueKey="id"
						labelKey="title"
						value={this.state.landingStatusFilter}
						options={this.props.billOfLandingReducer.listLandingStatusFilter}
						onChange={value => {this.props.setLandingStatusFilter(value.id);this.setState({landingStatusFilter: value.id})}}/>
				</div>
			</div>
		);
	}

	_renderContent(){
		if(!this.props.dataLoaded){
			return <WaitingMessage/>
		}
		return (
			<div>
				<div className="breadcrumb-container">
					{labels.common.title}
				</div>
				<StatusFilter/>
				<div className="main-content">
					{this._renderFilter()}
					<MainTable {...this.props}/>
				</div>

				<CustomModal
					open={this.props.mainModal}
					close={() => this.props.toggleModal(null, 'mainModal', false)}
					size="md"
					title="Quản lý vận đơn"
					>
					<div>
						<div className="custom-modal-content">
							<MainForm
								onSubmit={this.props.onChange}
								labels={labels.mainForm}
								submitTitle="Save">

								<button
									type="button"
									className="btn btn-warning cancel"
									onClick={() => this.props.toggleModal(null, 'mainModal', false)}>
									<span className="glyphicon glyphicon-remove"></span> &nbsp;
									Cancel
								</button>
							</MainForm>
						</div>
					</div>
				</CustomModal>
			</div>
		);
	}

	render() {
		return (
			<NavWrapper data-location={this.props.location} data-user={this.props.authReducer}>
				<div>
					{this._renderContent()}
				</div>
			</NavWrapper>
		);
	}
}

BillOfLandingLayout.propTypes = {
	bulkRemove: React.PropTypes.bool
};

BillOfLandingLayout.defaultProps = {
	bulkRemove: true
};

export default BillOfLandingLayout;
