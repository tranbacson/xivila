import React from 'react';
import Link from 'react-router/lib/Link';
import Tools from 'helpers/Tools';


class StatusFilter extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
	    };
	}

	render() {
		return (
			<div className="btn-group btn-group-justified" role="group">
				<Link
					activeClassName="active"
					to={Tools.toUrl('bill_of_landing', [2])}
					className="btn btn-default">Tất cả</Link>
				<Link
					activeClassName="active"
					to={Tools.toUrl('bill_of_landing', [0])}
					className="btn btn-default">Vận đơn thường</Link>
				<Link
					activeClassName="active"
					to={Tools.toUrl('bill_of_landing', [1])}
					className="btn btn-default">Vận đơn bảo hiểm</Link>
			</div>
		);
	}
}

StatusFilter.propTypes = {
};

StatusFilter.defaultProps = {
};

export default StatusFilter;
