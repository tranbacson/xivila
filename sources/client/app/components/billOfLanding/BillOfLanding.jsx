import React from 'react';
import isEmpty from 'lodash/isEmpty';
import keys from 'lodash/keys';
import values from 'lodash/values';
import filter from 'lodash/filter';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { SubmissionError, reset } from 'redux-form';
import * as actionCreators from 'app/actions/actionCreators';
import store from 'app/store';
import {apiUrls, labels} from './_data';
import Tools from 'helpers/Tools';
import BillOfLandingLayout from './BillOfLanding.layout';

class BillOfLanding extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			mainModal: false,
			itemId: null,
			bulkRemove: true,
			params: {},
			dataLoaded: store.getState().billOfLandingReducer.list.length?true:false,
			landing_status_filter: 'all'
	    };
	    this.list = this.list.bind(this);
	    this.toggleModal = this.toggleModal.bind(this);
	    this.handleFilter = this.handleFilter.bind(this);
	    this.handleCheck = this.handleCheck.bind(this);
	    this.handleCheckAll = this.handleCheckAll.bind(this);
	    this.handleRemove = this.handleRemove.bind(this);
	    this.handleChange = this.handleChange.bind(this);
	    this.handlePageChange = this.handlePageChange.bind(this);
	    this.setLandingStatusFilter = this.setLandingStatusFilter.bind(this);

	    this.filterTimeout = null;
	}

	setInitData(initData){
		const listAddress = initData.extra.list_address;
		const defaultAddressObj = filter(listAddress, {default: true})[0];
		let defaultAddress = null
		if(defaultAddressObj){
			defaultAddress = defaultAddressObj.id;
		}
		this.props.billOfLandingAction('newList', {list: [...initData.data.items], pages: initData.data._meta.last_page});
		this.props.billOfLandingAction('listAddress', {list: [...Tools.renameColumn(listAddress, 'address')]});
		this.props.billOfLandingAction('defaultAddress', defaultAddress);
		this.setState({dataLoaded: true});
	}

	componentDidMount(){
		document.title = 'BillOfLanding';
		// if(!this.props.billOfLandingReducer.list.length){
			if(window.initData){
		    	if(window.initData.success){
		    		this.setInitData(window.initData);
				}else{
					// Pop message here
				}
			    window.initData = null;
			}else{
				this.list();
			}
		// }
	}

	componentDidUpdate (prevProps) {
		let oldRegisterStatus = prevProps.params.insurance_register;
		let newRegisterStatus = this.props.params.insurance_register;
		if (newRegisterStatus !== oldRegisterStatus){
			this.list();
		}
	}

	toggleModal(id=null, state, open=true){
		let newState = {};
		newState[state] = open;

		switch(state){
			case 'mainModal':
				newState.itemId = id;
				if(id && open){
					Tools.apiCall(apiUrls.obj, {id: id}, false).then((result) => {
						if(result.success){
							result.data.landing_status = result.data.landing_status.split(':')[0];
							this.props.billOfLandingAction('obj', {
								...result.data
							});
							this.setState(newState);
							return;
						}
					});
				}else{
					this.props.billOfLandingAction('obj', {...Tools.getInitData(labels.mainForm), 'address_id': store.getState().billOfLandingReducer.defaultAddress});
					this.setState(newState);
				}
			break;
		}
	}


	list(outerParams={}, page=1){
	    let params = {
	    	insurance_register: this.props.params.insurance_register,
	    	landing_status_filter: this.state.landing_status_filter,
	    	page
	    };
		if(this.props.params.insurance_register === '2'){
			delete params.insurance_register;
		}

		if(!isEmpty(outerParams)){
	    	params = {...params, ...outerParams};
	    }

		Tools.apiCall(apiUrls.list, params, false).then((result) => {
	    	if(result.success){
		    	this.setInitData(result);
	    	}
	    });
	}

	setLandingStatusFilter(landing_status_filter){
		this.setState({landing_status_filter}, () => this.list());
	}

	handleFilter(event){
		let keyword = event.target.value;
		if(this.filterTimeout !== null){
			clearTimeout(this.filterTimeout);
		}
		this.filterTimeout = setTimeout(() => {
			if(keyword.length > 2){
				this.list({keyword: keyword});
			}else if(!keyword.length){
				this.list();
			}
		}, 600);
	}

	handlePageChange(data){
		let page = data.selected + 1;
		this.list({}, page);
	}

	handleCheck(id, checked){
		let index = store.getState().billOfLandingReducer.list.findIndex(x => x.id===id);
		this.props.billOfLandingAction('edit', {checked}, index);
	}

	handleCheckAll(){
		let list = this.props.billOfLandingReducer.list;
		if(filter(list, {checked: true}).length === list.length){
			this.props.billOfLandingAction('uncheckAll');
		}else{
			this.props.billOfLandingAction('checkAll');
		}
	}

	handleRemove(id=null){
		const confirm = window.confirm('Do you want to remove this item(s)?');
		if(!confirm){
			return;
		}
		if(id === null){
			let listId = [];
			this.props.billOfLandingReducer.list.map(value => {
				if(value.checked){
					listId.push(value.id);
				}
			});
			if(!listId.length){
				window.alert("Bạn vui lòng chọn ít nhất 1 phần tử để xoá.");
				return;
			}
			id = listId.join(',');
		}else{
			id = String(id);
		}
		Tools.apiCall(apiUrls.remove, {id}).then((result) => {
	    	if(result.success){
	    		let listId = result.data.id;
	    		if(typeof listId !== 'object'){
	    			listId = [listId];
	    		}
	    		let listIndex = listId.map(id => {
					return store.getState().billOfLandingReducer.list.findIndex(x => x.id === parseInt(id));
	    		});
				this.props.billOfLandingAction('remove', null, listIndex);
	    	}
	    });
	}

	handleChange(eventData, dispatch){
		try{
			const params = {...eventData};
			const id = this.state.itemId;
			return Tools.apiCall(apiUrls[id?'edit':'add'], id?{...params, id}:params).then((result) => {
		    	if(result.success){
		    		const data = {
						...result.data
		    		};
		    		if(id){
						let index = store.getState().billOfLandingReducer.list.findIndex(x => x.id===id);
						this.props.billOfLandingAction('edit', data, index);
		    		}else{
						this.props.billOfLandingAction('add', data);
		    		}
		    		dispatch(reset('BillOfLandingMainForm'));
		    		this.toggleModal(null, 'mainModal', false);
		    	}else{
					throw new SubmissionError(Tools.errorMessageProcessing(result.message));
		    	}
		    });
	    }catch(error){
			throw new SubmissionError(Tools.errorMessageProcessing(error));
		}
	}

	render() {
		return (
			<BillOfLandingLayout
				{...this.props}
				dataLoaded={this.state.dataLoaded}
				mainModal={this.state.mainModal}
				bulkRemove={this.state.bulkRemove}
				list={this.list}
				toggleModal={this.toggleModal}
				onFilter={this.handleFilter}
				setLandingStatusFilter={this.setLandingStatusFilter}
				onCheck={this.handleCheck}
				onCheckAll={this.handleCheckAll}
				onRemove={this.handleRemove}
				onChange={this.handleChange}
				onPageChange={this.handlePageChange}
				/>
		);
	}
}

function mapStateToProps(state){
	return {
	}
}

function mapDispatchToProps(dispatch){
	return {
		...bindActionCreators(actionCreators, dispatch),
		resetForm: (formName) => {
			dispatch(reset(formName));
		}
	};
}

BillOfLanding.propTypes = {
};

BillOfLanding.defaultProps = {
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(BillOfLanding);

// export default BillOfLanding;
