import Tools from 'helpers/Tools';
import {FIELD_TYPE} from 'app/constants';

const rawApiUrls = [
    {
        controller: 'billOfLanding',
        endpoints: {
            obj: 'GET',
            list: 'GET',
            add: 'POST',
            edit: 'POST',
            remove: 'POST'
        }
    }
];


export const labels = {
	common: {
		title: 'Quản lý vận đơn vận chuyển'
	},
    mainForm: {
        code: {
            title: 'Mã vận đơn',
            type: FIELD_TYPE.STRING,
            heading: true,
            init: null,
            rules: {
                required: true
            }
        },
        purchase_code: {
            title: 'Mã g.dịch',
            type: FIELD_TYPE.STRING,
            heading: true,
            init: null,
            rules: {
            }
        },
        address_code: {
            title: 'Mã địa chỉ',
            type: FIELD_TYPE.STRING,
            heading: true,
            init: null,
            rules: {
            }
        },
        landing_status: {
            title: 'T.Thái',
            type: FIELD_TYPE.STRING,
            heading: true,
            init: 'Mới',
            rules: {
            }
        },
        transform_factor: {
            title: 'Hệ số quy đổi',
            type: FIELD_TYPE.INTEGER,
            init: 6000,
            rules: {
                required: true
            }
        },
        insurance_register: {
            title: 'B.Hiểm',
            type: FIELD_TYPE.BOOLEAN,
            heading: true,
            init: null,
            rules: {
            }
        },
        address_id: {
            title: 'Địa chỉ',
            type: FIELD_TYPE.INTEGER,
            init: null,
            rules: {
                required: true
            }
        },
        length: {
            title: 'Dài',
            type: FIELD_TYPE.INTEGER,
            init: 0,
            rules: {
            }
        },
        width: {
            title: 'Rộng',
            type: FIELD_TYPE.INTEGER,
            init: 0,
            rules: {
            }
        },
        height: {
            title: 'Cao',
            type: FIELD_TYPE.INTEGER,
            init: 0,
            rules: {
            }
        },
        input_mass: {
            title: 'K.lượng',
            type: FIELD_TYPE.FLOAT,
            'suffix': 'Kg',
            init: 0,
            rules: {
            }
        },
        mass: {
            title: 'K.lượng',
            type: FIELD_TYPE.FLOAT,
            'suffix': 'Kg',
            heading: true,
            init: 0,
            rules: {
            }
        },
        packages: {
            title: 'Số kiện',
            type: FIELD_TYPE.INTEGER,
            heading: true,
            init: 1,
            rules: {
                required: true
            }
        },
        delivery_fee: {
            title: 'Phí V.Chuyển',
            type: FIELD_TYPE.FLOAT,
            prefix: '₫',
            heading: true,
            init: 0,
            rules: {
            }
        },
        insurance_fee: {
            title: 'Phí B.Hiểm',
            type: FIELD_TYPE.FLOAT,
            prefix: '₫',
            heading: true,
            init: 0,
            rules: {
            }
        },
        sub_fee: {
            title: 'Phụ phí',
            type: FIELD_TYPE.FLOAT,
            prefix: '₫',
            heading: true,
            init: 0,
            rules: {
            }
        },

        total: {
            title: 'Tổng cộng',
            type: FIELD_TYPE.INTEGER,
            prefix: '₫',
            heading: true,
            init: 0,
            rules: {
            }
        },
        insurance_value: {
            title: 'Giá trị thực',
            type: FIELD_TYPE.FLOAT,
            init: 0,
            rules: {
            }
        },
        note: {
            title: 'Ghi chú',
            type: FIELD_TYPE.STRING,
            heading: true,
            init: null,
            rules: {
            }
        }
    }
};

export const apiUrls = Tools.getApiUrlsV1(rawApiUrls);
