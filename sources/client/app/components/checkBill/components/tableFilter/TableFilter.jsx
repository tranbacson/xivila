import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from 'app/actions/actionCreators';

class TableFilter extends React.Component {
	constructor(props) {
		super(props);
	    this.onFilter = this.onFilter.bind(this);
	}

	onFilter(event){
		this.props.checkBillAction('keyword', event.target.value);
		this.props.onFilter(event);
	}

	render(){
		return (
			<div className="row">
				<div className="col-md-12">
					<input
						type="text"
						className="form-control"
						value={this.props.checkBillReducer.keyword}
						onChange={this.onFilter}
						placeholder="Mã vận đơn..."/>
				</div>
			</div>
		)
	}
}
TableFilter.propTypes = {
	onFilter: React.PropTypes.func.isRequired
};
TableFilter.defaultProps = {
};

function mapStateToProps(state){
	return {
		checkBillReducer: state.checkBillReducer
	}
}

function mapDispatchToProps(dispatch){
	return {
		...bindActionCreators(actionCreators, dispatch),
		resetForm: (formName) => {
			dispatch(reset(formName));
		}
	};
}

TableFilter.propTypes = {
};

TableFilter.defaultProps = {
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(TableFilter);