import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from 'app/actions/actionCreators';
import values from 'lodash/values';
import forEach from 'lodash/forEach';
import {ADMIN_ROLES} from 'app/constants';
import Tools from 'helpers/Tools';
import {apiUrls} from '../../_data';
import OrderItemRow from '../orderItemRow/OrderItemRow';


class PurchaseRow extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			billOfLandingCode: this.props.billOfLandingCode,
			changeBillOfLandingCode: false
		};
		this._renderBillOfLanding = this._renderBillOfLanding.bind(this);
		this.handleChangeBillOfLandingCode = this.handleChangeBillOfLandingCode.bind(this);
		this.handleConfirmCode = this.handleConfirmCode.bind(this);
		this.handleToggleCheckAll = this.handleToggleCheckAll.bind(this);
		this.discardChangeCode = this.discardChangeCode.bind(this);
	}

	componentWillReceiveProps(nextProps){
		if(this.props.dataSession !== nextProps.dataSession){
			this.discardChangeCode();
		}
	}

	handleChangeBillOfLandingCode(event){
		this.setState({billOfLandingCode: event.target.value});
	}

	discardChangeCode(index=null, purchaseCode=null){
		this.setState({changeBillOfLandingCode: false});
		this.setState({billOfLandingCode: this.props.billOfLandingCode});
		if(index !== null && purchaseCode !== null){
			this.props.checkBillAction('updatePurchase', {bill_of_landing_code: this.props.billOfLandingCode}, index);
		}
	}

	handleConfirmCode(index){
		const billOfLandingCode = this.state.billOfLandingCode;
		const purchaseCode = this.props.purchase.code;

		if(billOfLandingCode !== this.props.billOfLandingCode){

			const changeState = !this.state.changeBillOfLandingCode;
			this.setState({changeBillOfLandingCode: changeState});
			if(changeState){
				let data = {};
				data[purchaseCode] = billOfLandingCode;
				this.props.checkBillAction('updatePurchase', {bill_of_landing_code: billOfLandingCode}, index);

				Tools.apiCall(apiUrls.billOfLandingCheckDuplicateCode, {code: billOfLandingCode}, false).then(result => {
					if(result.success && result.data.duplicate){
						this.discardChangeCode(index, purchaseCode);
						window.alert("Mã vận đơn này đã được sử dụng. Bạn vui lòng chọn mã vận đơn khác.");
					}
				});

			}else{
				// Restore to normal
				this.discardChangeCode(index, purchaseCode);
			}
		}else{
			this.discardChangeCode(index, purchaseCode);
		}
	}

	handleChangePurchase(index, key, value){
		if(key === 'mass'){
			value = parseFloat(value?value:0);
		}else if(['packages', 'length', 'width', 'height', 'sub_fee'].indexOf(key) !== -1){
			value = parseInt(value?value:0);
		}
		let data = {};
		data[key] = value;
		this.props.checkBillAction('updatePurchase', data, index);
	}

	handleToggleCheckAll(index){
		let numberOfItem = this.props.purchase.order_items.length;
		let numberOfMatch = 0;
		let numberOfZero = 0;
		forEach(this.props.purchase.order_items, item =>{
			if(item.quantity === item.checked_quantity){
				numberOfMatch++;
			}
			if(!item.checked_quantity){
				numberOfZero++;
			}
		});
		if(numberOfMatch === numberOfItem){
			this.props.checkBillAction('uncheckAllItemOfPurchase', null, index);
		}else{
			this.props.checkBillAction('checkAllItemOfPurchase', null, index);
		}
	}

	_renderBillOfLanding(){
		return (
			<div>
				<div className="row">
					<div className="col-md-3">
						<div className="input-group">
							<input
								type="number"
								step="0.1"
								value={this.props.purchase.input_mass}
								onChange={e => {this.handleChangePurchase(this.props.index, 'input_mass', e.target.value)}}
								className="form-control"
								placeholder="K.Lượng"/>
							<span className="input-group-addon">Kg</span>
					    </div>
					</div>
					<div className="col-md-3">
						<div className="input-group">
							<input
								type="number"
								value={this.props.purchase.packages}
								onChange={e => {this.handleChangePurchase(this.props.index, 'packages', e.target.value)}}
								className="form-control"
								placeholder="Kiện"/>
							<span className="input-group-addon">Kiện</span>
					    </div>
					</div>
					<div className="col-md-6">
						<div className="input-group">
							<input
								type="text"
								className={"form-control"+(this.state.changeBillOfLandingCode?" red":"")}
								value={this.state.billOfLandingCode}
								onChange={this.handleChangeBillOfLandingCode}
								onKeyPress={e => {if(e.key === 'Enter'){this.handleConfirmCode(this.props.index)} }}
								placeholder="Mã vận đơn..."/>
							<span className="input-group-btn">
								<button
									onClick={e => this.handleConfirmCode(this.props.index)}
									className={this.state.changeBillOfLandingCode?"btn btn-danger":"btn btn-default"}
									type="button">
									<span className={this.state.changeBillOfLandingCode?"glyphicon glyphicon-remove":"glyphicon glyphicon-ok"}></span>
								</button>
							</span>
					    </div>
					</div>
				</div>
				<div className="row">
					<div className="col-md-3">
						<div className="input-group">
							<span className="input-group-addon">dài</span>
							<input
								type="number"
								value={this.props.purchase.length}
								onChange={e => {this.handleChangePurchase(this.props.index, 'length', e.target.value)}}
								className="form-control"
								placeholder="Dài"/>
					    </div>
					</div>
					<div className="col-md-3">
						<div className="input-group">
							<span className="input-group-addon">rộng</span>
							<input
								type="number"
								value={this.props.purchase.width}
								onChange={e => {this.handleChangePurchase(this.props.index, 'width', e.target.value)}}
								className="form-control"
								placeholder="Rộng"/>
					    </div>
					</div>
					<div className="col-md-3">
						<div className="input-group">
							<span className="input-group-addon">cao</span>
							<input
								type="number"
								value={this.props.purchase.height}
								onChange={e => {this.handleChangePurchase(this.props.index, 'height', e.target.value)}}
								className="form-control"
								placeholder="Cao"/>
					    </div>
					</div>
					<div className="col-md-3">
						<div className="input-group">
							<span className="input-group-addon">P.Phí</span>
							<input
								type="number"
								value={this.props.purchase.sub_fee}
								onChange={e => {this.handleChangePurchase(this.props.index, 'sub_fee', e.target.value)}}
								className="form-control"
								placeholder="P.Phí"/>
					    </div>
					</div>
				</div>
			</div>
		);
	}

	_renderItem(purchase, listItem){
		return listItem.map((row, i) => {
			return (
				<OrderItemRow
					{...this.props}
					key={i}
					index={i}
					purchaseIndex={this.props.index}
					purchase={purchase}
					item={row}/>
			);
		});
	}

	render(){
		return (
			<tbody>
				<tr className="cyan-bg">
					<td colSpan={2} className="white" style={{verticalAlign: 'middle'}}>
						<div>
							<span
								onClick={e => this.handleToggleCheckAll(this.props.index)}
								className="glyphicon glyphicon-ok green pointer"></span>&nbsp;&nbsp;&nbsp;
							<strong>
								{this.props.purchase.order_uid}
							</strong>
							&nbsp;&rarr;&nbsp;
							<strong>
								{this.props.purchase.code}
							</strong>
						</div>
						<div>
							<input
								type="text"
								value={this.props.purchase.note}
								onChange={e => {this.handleChangePurchase(this.props.index, 'note', e.target.value)}}
								className="form-control"
								placeholder="Ghi chú..."/>
						</div>
					</td>
					<td style={{verticalAlign: 'middle'}}>
						<button
							type="button"
							className="btn btn-success btn-block"
							onClick={() => this.props.onChange(this.props.purchase)}>
							<span className="glyphicon glyphicon-ok"></span>
						</button>
					</td>
					<td className="white">
						{this._renderBillOfLanding()}
					</td>
				</tr>
				{this._renderItem(this.props.purchase, this.props.purchase.order_items)}
			</tbody>
		);
	}
}

PurchaseRow.propTypes = {
	purchase: React.PropTypes.object.isRequired
};

PurchaseRow.defaultProps = {
};


function mapStateToProps(state){
	return {
	}
}

function mapDispatchToProps(dispatch){
	return {
		...bindActionCreators(actionCreators, dispatch)
	};
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(PurchaseRow);
