import React from 'react';
import values from 'lodash/values';
import forEach from 'lodash/forEach';
import {labels} from '../../_data';
import Tools from 'helpers/Tools';


class OrderItemRow extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.handleChangeCheckedQuantity = this.handleChangeCheckedQuantity.bind(this)
	}

	handleChangeCheckedQuantity(quantity){
		this.handleChangeCheckedQuantity = this.handleChangeCheckedQuantity.bind(this);
		const data = {
			checked_quantity: parseInt(quantity)
		};
		this.props.checkBillAction('checkedQuantity', data, this.props.index, this.props.purchaseIndex);
	}

	render(){
		return (
			<tr>
				<td className="grid-thumbnail">
					<img
						className="pointer"
						onClick={() => {this.props.toggleModal(this.props.item.avatar, 'previewModal')}}
						src={this.props.item.avatar}
						width="100%"/>
				</td>
				<td>
					<div>
						{this.props.item.title}
					</div>
					<div className="cyan">
						{this.props.item.properties}
					</div>
					<div>
						<em>
							{this.props.item.message}
						</em>
					</div>
				</td>

				<td className="right-align" style={{verticalAlign: 'middle'}}>
					<strong>
						{this.props.item.remain_quantity}/{this.props.item.quantity}
					</strong>
				</td>

				<td style={{verticalAlign: 'middle'}}>
					<input
						type="number"
						className="form-control"
						value={this.props.item.checked_quantity}
						onChange={e => {this.handleChangeCheckedQuantity(e.target.value, this.props.item.id, this.props.item.purchase_id)}}
						placeholder="Số lượng..."/>
				</td>
			</tr>
		);
	}
}

OrderItemRow.propTypes = {
	purchase: React.PropTypes.object.isRequired,
	item: React.PropTypes.object.isRequired
};

OrderItemRow.defaultProps = {
};

export default OrderItemRow;
