import Tools from 'helpers/Tools';
import {FIELD_TYPE} from 'app/constants';

const rawApiUrls = [
    {
        controller: 'order',
        endpoints: {
            obj: 'GET',
            list: 'GET',
            add: 'POST',
            edit: 'POST',
            remove: 'POST'
        }
    }, {
        controller: 'orderItem',
        endpoints: {
            obj: 'GET',
            edit: 'POST',
            remove: 'POST'
        }
    }, {
        controller: 'purchase',
        endpoints: {
            obj: 'GET',
            edit: 'POST'
        }
    }, {
        controller: 'billOfLanding',
        endpoints: {
            obj: 'GET',
            add: 'POST',
            edit: 'POST',
            remove: 'POST'
        }
    }
];

export const labels = {
	common: {
		title: 'Chi tiết đơn hàng'
	},
    mainForm: {
        title: {
            title: 'Tên sản phẩm',
            type: FIELD_TYPE.STRING,
            heading: true,
            init: null,
            rules: {
                required: true
            }
        },
        url: {
            title: 'Link sản phẩm',
            type: FIELD_TYPE.STRING,
            heading: true,
            init: null,
            rules: {
                required: true
            }
        },
        avatar: {
            title: 'Ảnh sản phẩm',
            type: FIELD_TYPE.STRING,
            heading: true,
            init: null,
            rules: {
            }
        },
        properties: {
            title: 'Các thuộc tính',
            type: FIELD_TYPE.STRING,
            heading: true,
            init: null,
            rules: {
            }
        },
        unit_price: {
            title: 'Đơn giá CNY',
            type: FIELD_TYPE.FLOAT,
            heading: true,
            init: null,
            rules: {
                required: true
            }
        },
        quantity: {
            title: 'Số lượng',
            type: FIELD_TYPE.INTEGER,
            heading: true,
            init: null,
            rules: {
                required: true
            }
        },
        message: {
            title: 'Ghi chú',
            type: FIELD_TYPE.STRING,
            heading: true,
            init: null,
            rules: {
            }
        }
    }, realAmountForm: {
        real_amount: {
            title: 'Thanh toán thực',
            type: FIELD_TYPE.FLOAT,
            init: 0,
            rules: {
                required: true
            }
        }
    }, deliveryFeeForm: {
        delivery_fee_unit: {
            title: 'Đơn giá vận chuyển',
            type: FIELD_TYPE.FLOAT,
            init: 0,
            rules: {
                required: true
            }
        },
        inland_delivery_fee_raw: {
            title: 'Phí vận chuyển nội địa',
            type: FIELD_TYPE.FLOAT,
            init: 0,
            rules: {
                required: true
            }
        }
    }, purchaseCodeForm: {
        code: {
            title: 'Mã giao dịch',
            type: FIELD_TYPE.STRING,
            init: '',
            rules: {
            }
        }
    }, purchaseNoteForm: {
        code: {
            title: 'Ghi chú',
            type: FIELD_TYPE.STRING,
            init: '',
            rules: {
            }
        }
    }, billOfLandingForm: {
        code: {
            title: 'Mã vận đơn',
            type: FIELD_TYPE.STRING,
            init: '',
            rules: {
                required: true
            }
        },
        packages: {
            title: 'Số kiện',
            type: FIELD_TYPE.INTEGER,
            init: 1,
            rules: {
                required: true
            }
        },
        transform_factor: {
            title: 'Hệ số quy đổi',
            type: FIELD_TYPE.INTEGER,
            init: 6000,
            rules: {
                required: true
            }
        },
        input_mass: {
            title: 'Khối lượng',
            type: FIELD_TYPE.FLOAT,
            init: 0,
            rules: {
            }
        },
        length: {
            title: 'Dài',
            type: FIELD_TYPE.INTEGER,
            init: 0,
            rules: {
            }
        },
        width: {
            title: 'Rộng',
            type: FIELD_TYPE.INTEGER,
            init: 0,
            rules: {
            }
        },
        height: {
            title: 'Cao',
            type: FIELD_TYPE.INTEGER,
            init: 0,
            rules: {
            }
        },
        insurance_register: {
            title: 'Đồng ý đăng ký bảo hiểm',
            type: FIELD_TYPE.BOOLEAN,
            init: false,
            rules: {
            }
        },
        insurance_value: {
            title: 'Giá trị thực',
            type: FIELD_TYPE.FLOAT,
            init: 0,
            rules: {
            }
        }
    }, addressForm: {
        address_id: {
            title: 'Địa chỉ nhận hàng',
            type: FIELD_TYPE.INTEGER,
            init: null,
            rules: {
                required: true
            }
        }
    }, statusForm: {
        status: {
            title: 'Trạng thái đơn hàng',
            type: FIELD_TYPE.STRING,
            init: 'new',
            rules: {
                required: true
            }
        }
    }, adminForm: {
        admin_id: {
            title: 'Nhân viên mua hàng',
            type: FIELD_TYPE.INTEGER,
            init: null,
            rules: {
            }
        }
    }
};

export const apiUrls = Tools.getApiUrlsV1(rawApiUrls);
