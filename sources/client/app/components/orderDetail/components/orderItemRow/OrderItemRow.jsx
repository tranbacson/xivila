import React from 'react';
import values from 'lodash/values';
import forEach from 'lodash/forEach';
import {labels} from '../../_data';
import Tools from 'helpers/Tools';
import PurchaseInfo from '../purchaseInfo/PurchaseInfo';


class OrderItemRow extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render(){
		return (
			<tr>
				<td>{this.props.index + 1}</td>
				<td className="grid-thumbnail">
					<img
						className="pointer"
						onClick={() => {this.props.toggleModal(this.props.item.avatar, 'previewModal')}}
						src={this.props.item.avatar}
						width="100%"/>
				</td>
				<td>
					<div>
						<a href={this.props.item.url} target="_blank">
							{this.props.item.title}
						</a>
					</div>
					<div className="cyan">
						{this.props.item.properties}
					</div>
					<div>
						<em>
							{this.props.item.message}
						</em>
					</div>
				</td>

				<td className="right-align">
					<div>
						{this.props.item.quantity}
						&nbsp;x&nbsp;
						{Tools.numberFormat(this.props.item.unit_price)}
						&nbsp;=&nbsp;
						<strong>
							{Tools.numberFormat(parseFloat(this.props.item.unit_price) * parseInt(this.props.item.quantity))} ￥
						</strong>
					</div>
					<div>
						<strong>
							{Tools.numberFormat(parseFloat(this.props.item.unit_price) * parseInt(this.props.item.quantity) * this.props.rate)} ₫
						</strong>
					</div>
				</td>
				<PurchaseInfo {...this.props}/>

				<td>
					<span
						onClick={() => {this.props.toggleModal(this.props.item.id, 'itemModal')}}
						className="glyphicon glyphicon-pencil"></span>
				</td>
				<td>
					<span
						onClick={() => {this.props.onRemove(this.props.item.id)}}
						className="glyphicon glyphicon-remove red"></span>
				</td>
			</tr>
		);
	}
}

OrderItemRow.propTypes = {
	index: React.PropTypes.number.isRequired,
	purchase: React.PropTypes.object.isRequired,
	numberOfItems: React.PropTypes.number.isRequired,
	item: React.PropTypes.object.isRequired,
	rate: React.PropTypes.number.isRequired
};

OrderItemRow.defaultProps = {
};

export default OrderItemRow;
