import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from 'app/actions/actionCreators';
import store from 'app/store';
import values from 'lodash/values';
import forEach from 'lodash/forEach';
import {ADMIN_ROLES} from 'app/constants';
import {labels} from '../../_data';
import Tools from 'helpers/Tools';


class ListBillOfLanding extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
		this._renderItem = this._renderItem.bind(this);
		this._renderBillOfLandingCode = this._renderBillOfLandingCode.bind(this);
	}

	_renderBillOfLandingCode(item){
		if(ADMIN_ROLES.indexOf(Tools.getStorage('authData').role) !== -1){
			return (
				<div>
					<span
						onClick={() => {
							this.props.orderDetailAction('selectedShop', item.purchase_id);
							this.props.toggleModal(item.id, 'billOfLandingModal')
						}}
						className="pointer green">
						{item.code}
					</span>
					&nbsp;
					<span
						onClick={() => {this.props.onRemoveCode(item.id)}}
						className="glyphicon glyphicon-remove pointer red"></span>
				</div>
			);
		}else{
			return (
				<div>
					<span
						className="pointer green">
						{item.code}
					</span>
				</div>
			);
		}
	}

	_renderItem(){
		return this.props.listItem.map((item, i) => {
			return(
				<div key={i}>
					{this._renderBillOfLandingCode(item)}
					<ul className="thin-padding">
						<li>
							{item.packages}	Kiện / {item.mass}	Kg
						</li>
					</ul>
				</div>
			);
		});
	}

	render(){
		return (
			<div>
				{this._renderItem()}
			</div>
		);

	}
}

function mapStateToProps(state){
	return {
	}
}

function mapDispatchToProps(dispatch){
	return {
		...bindActionCreators(actionCreators, dispatch)
	};
}

ListBillOfLanding.propTypes = {
	purchaseId: React.PropTypes.number.isRequired,
	listItem: React.PropTypes.array.isRequired
};

ListBillOfLanding.defaultProps = {
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ListBillOfLanding);

// export default ListBillOfLanding;
