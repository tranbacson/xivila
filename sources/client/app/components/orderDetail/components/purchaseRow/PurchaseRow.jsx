import React from 'react';
import values from 'lodash/values';
import forEach from 'lodash/forEach';
import {ADMIN_ROLES} from 'app/constants';
import {labels} from '../../_data';
import Tools from 'helpers/Tools';
import OrderItemRow from '../orderItemRow/OrderItemRow';


class PurchaseRow extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
		this._renderRealAmountLabel = this._renderRealAmountLabel.bind(this);
		this._renderRealAmountValue = this._renderRealAmountValue.bind(this);
		this._renderDeliveryFeeEditButton = this._renderDeliveryFeeEditButton.bind(this);
	}

	_renderItem(purchase, listItem, shopIndex){
		return listItem.map((row, i) => {
			return (
				<OrderItemRow
					{...this.props}
					key={i}
					index={i}
					purchase={purchase}
					numberOfItems={listItem.length}
					item={row}
					rate={this.props.rate}/>
			);
		});
	}

	_renderNumber($input, prefix){
		if($input < 0.001){
			return <span>Chưa cập nhật</span>
		}
		return prefix + Tools.numberFormat(parseFloat($input));
	}

	_renderDeliveryFeeEditButton(){
		if(ADMIN_ROLES.indexOf(Tools.getStorage('authData').role) !== -1){
			return(
				<span
					onClick={() => {this.props.toggleModal(this.props.purchase.id, 'deliveryFeeModal')}}
					className="glyphicon glyphicon-pencil pointer"></span>
			);
		}
	}

	_renderRealAmountLabel(){
		if(ADMIN_ROLES.indexOf(Tools.getStorage('authData').role) !== -1){
			return(
				<div className="strong white">
					Thanh toán thực
					&nbsp;
					<span
						onClick={() => {this.props.toggleModal(this.props.purchase.id, 'realAmountModal')}}
						className="glyphicon glyphicon-pencil pointer"></span>
				</div>
			);
		}
	}

	_renderRealAmountValue(){
		if(ADMIN_ROLES.indexOf(Tools.getStorage('authData').role) !== -1){
			return(
				<div className="strong white">
					{this._renderNumber(this.props.purchase.real_amount, '￥')}
				</div>
			);
		}
	}

	render(){
		return (
			<tbody>
				<tr className="cyan-bg">
					<td colSpan={3} className="white">
						<strong>
							<span
								onClick={()=>this.props.onToggleLog(this.props.purchase.id)}
								className="glyphicon glyphicon-th-large pointer green"></span>&nbsp;
							[{this.props.purchase.vendor}] {this.props.purchase.title}
						</strong>
					</td>
					<td className="white right-align">
						<div>
							<strong>
								{Tools.numberFormat(this.props.purchase.amount)} ￥
							</strong>
						</div>
						<div>
							<strong>
								{Tools.numberFormat(this.props.purchase.amount*this.props.rate)} ₫
							</strong>
						</div>
					</td>
					<td colSpan={2}>
						<div className="strong white">
							Phí vận chuyển
							&nbsp;
							{this._renderDeliveryFeeEditButton()}
						</div>
						{this._renderRealAmountLabel()}
					</td>
					<td colSpan={2}>
						<div className="strong white">
							{this._renderNumber(this.props.purchase.delivery_fee, '₫')} +
							{this._renderNumber(this.props.purchase.inland_delivery_fee_raw, '￥')}
						</div>
						{this._renderRealAmountValue()}
					</td>
				</tr>
				{this._renderItem(this.props.purchase, this.props.purchase.order_items, this.props.index)}
			</tbody>
		);
	}
}

PurchaseRow.propTypes = {
	index: React.PropTypes.number.isRequired,
	purchase: React.PropTypes.object.isRequired,
	rate: React.PropTypes.number.isRequired
};

PurchaseRow.defaultProps = {
};

export default PurchaseRow;
