import React from 'react';
import values from 'lodash/values';
import forEach from 'lodash/forEach';
import {labels} from '../_data';
import Tools from 'helpers/Tools';
import PurchaseRow from '../components/purchaseRow/PurchaseRow';


class MainTable extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
		this._renderShop = this._renderShop.bind(this);
	}

	_renderShop(){
		if(this.props.listItem.length){
			return this.props.listItem.map((row, i) =>{
				return (
					<PurchaseRow
						{...this.props}
						key={i}
						index={i}
						purchase={row}
						rate={this.props.rate}/>
				);
			});
		}
	}

	render(){
		return (
			<table className="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Stt</th>
						<th>Ảnh</th>
						<th>Tên sản phẩm</th>
						<th className="right-align">Thành tiền</th>
						<th colSpan={2}>Thông tin purchase</th>
						<th colSpan={2}>
							<button
								type="button"
								className="btn btn-success btn-xs btn-block"
								onClick={() => {this.props.toggleModal(null, 'mainModal')}}>
								<span className="glyphicon glyphicon-plus"></span>&nbsp;
								S.phẩm
							</button>
						</th>
					</tr>
				</thead>
				{this._renderShop()}
			</table>
		);
	}
}

MainTable.propTypes = {
};

MainTable.defaultProps = {
};

export default MainTable;
