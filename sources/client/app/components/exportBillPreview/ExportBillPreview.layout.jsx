import React from 'react';
import values from 'lodash/values';
import forEach from 'lodash/forEach';
import Table from 'rc-table';
import {labels} from './_data';
import Tools from 'helpers/Tools';
import NavWrapper from 'utils/components/NavWrapper';
import CustomModal from 'utils/components/CustomModal';
import WaitingMessage from 'utils/components/WaitingMessage';
import { SubmissionError, reset } from 'redux-form';
import DeliveryNote from './components/deliveryNote/DeliveryNote';


class ExportBillPreviewLayout extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	_renderContent(){
		if(!this.props.dataLoaded){
			return <WaitingMessage/>
		}
		return (
			<div>
				<div className="breadcrumb-container non-printable">
					{labels.common.title}
				</div>
				<div className="main-content">
					<DeliveryNote
						data={this.props.exportBillPreviewReducer.obj}/>
				</div>

			</div>
		);
	}

	render() {
		return (
			<NavWrapper data-location={this.props.location} data-user={this.props.authReducer}>
				<div>
					{this._renderContent()}
				</div>
			</NavWrapper>
		);
	}
}

ExportBillPreviewLayout.propTypes = {
	bulkRemove: React.PropTypes.bool
};

ExportBillPreviewLayout.defaultProps = {
	bulkRemove: true
};

export default ExportBillPreviewLayout;
