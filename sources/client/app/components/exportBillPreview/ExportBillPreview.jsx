import React from 'react';
import isEmpty from 'lodash/isEmpty';
import keys from 'lodash/keys';
import values from 'lodash/values';
import filter from 'lodash/filter';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { SubmissionError, reset } from 'redux-form';
import * as actionCreators from 'app/actions/actionCreators';
import store from 'app/store';
import {apiUrls, labels} from './_data';
import Tools from 'helpers/Tools';
import ExportBillPreviewLayout from './ExportBillPreview.layout';

class ExportBillPreview extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			itemId: null,
			params: {},
			dataLoaded: false
	    };
	}

	setInitData(initData){
		this.props.exportBillPreviewAction('obj', {...initData.data, ...initData.extra});
		this.setState({dataLoaded: true});
	}

	componentDidMount(){
		document.title = 'ExportBillPreview';
		const parentNode = document.getElementsByClassName('content-wrapper')[0].parentNode;
		parentNode.setAttribute('id', 'content-wrapper-parent');
		if(window.initData){
	    	if(window.initData.success){
	    		this.setInitData(window.initData);
			}else{
				// Pop message here
			}
		    window.initData = null;
		}else{
			this.getInitData();
		}
	}

	getInitData(){
		Tools.apiCall(apiUrls.obj, {id: this.props.params.id}, false).then((result) => {
	    	if(result.success){
		    	this.setInitData(result);
	    	}
	    });
	}

	render() {
		return (
			<ExportBillPreviewLayout
				{...this.props}
				dataLoaded={this.state.dataLoaded}
				/>
		);
	}
}

function mapStateToProps(state){
	return {
	}
}

function mapDispatchToProps(dispatch){
	return {
		...bindActionCreators(actionCreators, dispatch),
		resetForm: (formName) => {
			dispatch(reset(formName));
		}
	};
}

ExportBillPreview.propTypes = {
};

ExportBillPreview.defaultProps = {
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ExportBillPreview);

// export default ExportBillPreview;
