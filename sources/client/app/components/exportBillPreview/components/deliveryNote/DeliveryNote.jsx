import React from 'react';
import Barcode from 'react-barcode';
import Tools from 'helpers/Tools';


class DeliveryNote extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			listBillOfLanding: [],
			totalInsurance: 0,
		    totalSubFee: 0,
		    totalDeliveryFee: 0,
		    totalPackages: 0,
		    totalMass: 0,
		    totalAmount: 0,
		    total: 0,
		    listAddress: []
	    };
	    this._renderBillOfLanding = this._renderBillOfLanding.bind(this);
	}


	componentDidMount(){
		let totalInsurance = 0;
	    let totalSubFee = 0;
	    let totalDeliveryFee = 0;
	    let totalPackages = 0;
	    let totalMass = 0;
	    let totalAmount = 0;
	    let total = 0;
	    let listAddress = [];

		const listBillOfLanding = this.props.data.list_bill_of_landing.map((item, index) => {
			if(listAddress.indexOf(item.address_code) === -1){
				listAddress.push(item.address_code);
			}
			item.insurance_fee = parseInt(item.insurance_fee);
			item.delivery_fee = parseInt(item.delivery_fee);
			item.sub_fee = parseInt(item.sub_fee);

			totalInsurance += item.insurance_fee;
			totalDeliveryFee += item.delivery_fee;
			totalSubFee += item.sub_fee;
			totalPackages += parseInt(item.packages);
			totalMass += parseFloat(item.mass);
			totalAmount += parseInt(item.amount) + parseInt(item.order_fee);
			total += parseInt(item.total);
			return item;
		});

		this.setState({
			listAddress,
			listBillOfLanding,
			totalInsurance,
			totalDeliveryFee,
			totalSubFee,
			totalPackages,
			totalMass,
			totalAmount,
			total
		});
	}

	_renderBillOfLanding(){
		return this.state.listBillOfLanding.map((billOfLanding, index) => {
			return(
				<tr key={index}>
					<td>{index + 1}</td>
					<td>{billOfLanding.code}</td>
					<td>{billOfLanding.address_code}</td>
					<td className={billOfLanding.packages>1?'strong':''}>
						{billOfLanding.packages} - {billOfLanding.mass}Kg
					</td>
					<td>
						{Tools.numberFormat(billOfLanding.insurance_fee)}
					</td>
					<td>
						{Tools.numberFormat(billOfLanding.sub_fee)}
					</td>
					<td>
						{Tools.numberFormat(billOfLanding.delivery_fee)}
					</td>
					<td>
						{Tools.numberFormat(parseFloat(billOfLanding.total_raw) * parseInt(billOfLanding.rate))}
					</td>
					<td className="non-printable">
						{Tools.numberFormat(billOfLanding.total)}
					</td>
					<td>{billOfLanding.note}</td>
				</tr>
			);
		});
	}

	render() {
		return (
			<div className="row">
				<div className="col-xs-12">
					<div className="non-printable">
						<button
							onClick={() => {window.print()}}
							className="btn btn-success btn-block">
							<span className="glyphicon glyphicon-print"></span>&nbsp;
							In phiếu giao hàng
						</button>
					</div>
					<div className="printable">
						<h2 className="center-align">PHIẾU GIAO HÀNG</h2>
						<div className="row">
							<div className="col-sm-8">
								{Tools.dateFormat(this.props.data.created_at)} | <strong>{this.state.listAddress.join(', ')}</strong>
							</div>
							<div className="col-sm-4 right-align">
								<Barcode
									value={this.props.data.uid}
									height={20}
									fontSize={12}
									/>
							</div>
						</div>
						<table className="table table-bordered">
							<thead>
								<tr>
									<th>STT</th>
									<th>Mã bill</th>
									<th>Địa chỉ</th>
									<th>Số kiện</th>
									<th>Bảo hiểm</th>
									<th>Phụ phí</th>
									<th>Vận Chuyển</th>
									<th>Tiền hàng</th>
									<th className="non-printable">Tổng</th>
									<th>Ghi chú</th>
								</tr>
							</thead>
							<tbody>
								{this._renderBillOfLanding()}
							</tbody>
							<tfoot>
								<tr>
									<td colSpan={3}>
										<strong>
											Tổng cộng
										</strong>
									</td>
									<td>
										{Tools.numberFormat(this.state.totalPackages)} - {Tools.numberFormat(this.state.totalMass)}Kg
									</td>
									<td>
										{Tools.numberFormat(this.state.totalInsurance)}
									</td>
									<td>
										{Tools.numberFormat(this.state.totalSubFee)}
									</td>
									<td>
										{Tools.numberFormat(this.state.totalDeliveryFee)}
									</td>
									<td>
										{Tools.numberFormat(this.state.totalAmount)}
									</td>
									<td className="non-printable">
										{Tools.numberFormat(this.state.total)}
									</td>
									<td></td>
								</tr>
							</tfoot>
						</table>
						<div className="row">
							<div className="col-xs-6">
								Phí giao hàng
							</div>
							<div className="col-xs-6 right-align">
								<strong>
									{Tools.numberFormat(this.props.data.sub_fee)} VND
								</strong>
							</div>
						</div>
						<br/>
						<div className="row">
							<div className="col-xs-6">
								Tổng cộng
							</div>
							<div className="col-xs-6 right-align">
								<strong>
									{Tools.numberFormat(this.state.total + this.props.data.sub_fee)} VND
								</strong>
							</div>
						</div>
						<br/>
						<div className="row">
							<div className="col-xs-2">
								Ghi chú
							</div>
							<div className="col-xs-10">
								<div className="dot-underline">{this.props.data.note}</div>
								<div className="dot-underline">&nbsp;</div>
								<div className="dot-underline">&nbsp;</div>
							</div>
						</div>
						<br/>
						<div>
							<em>Ngày........Tháng........Năm........</em>
						</div>
						<br/>
						<div className="row">
							<div className="col-xs-6 center-align">
								<strong>
									Bên giao
								</strong>
							</div>
							<div className="col-xs-6 center-align">
								<strong>
									Bên nhận
								</strong>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}



DeliveryNote.propTypes = {
	// orderId: React.PropTypes.number.isRequired,
};

DeliveryNote.defaultProps = {

};

export default DeliveryNote;
