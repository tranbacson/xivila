import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { SubmissionError, reset } from 'redux-form';
import md5 from 'blueimp-md5';

// App components
import { ADMIN_ROLES } from 'app/constants';
import * as actionCreators from 'app/actions/actionCreators';
import ProfileForm from './forms/Profile.form';
import ChangePasswordForm from './forms/ChangePassword.form';
import Tools from 'helpers/Tools';
import {apiUrls, labels} from './_data';

import NavWrapper from 'utils/components/NavWrapper';
import CustomModal from 'utils/components/CustomModal';


class Profile extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			profileModal: false,
			changePasswordModal: false,
			extensionUrlGrab: null
	    };
	    this._renderFullName = this._renderFullName.bind(this);
	    this._renderInfo = this._renderInfo.bind(this);
	    this._renderForAdmin = this._renderForAdmin.bind(this);
	    this._renderForUser = this._renderForUser.bind(this);
	}

	componentDidMount(){
		document.title = 'Profile';
		Tools.apiCall(apiUrls.profile, {}, false).then((result) => {
	    	if(result.success){
	    		this.setState({extension_url_grab: result.extra.extension_url_grab});
		    	this.props.updateProfile({...result.data});
	    		/*
		    	this.props.updateProfile({
		    		email: result.data.email,
		    		first_name: result.data.first_name,
		    		last_name: result.data.last_name,
		    		role_id: result.data.role_id
		    	});
		    	*/
	    	}
	    });
	}

	toggleModal(state, value=true){
		let newState = {};
		newState[state] = value;
		this.setState(newState);
	}

	updateProfileHandle(eventData, dispatch){
		try{
			const params = {
				...eventData
			};
			return Tools.apiCall(apiUrls.updateProfile, params).then((result) => {
		    	if(result.success){
					Tools.setStorage('authData', result.data);
		    		this.props.updateProfile({
		    			email: result.data.email,
			    		first_name: result.data.first_name,
			    		last_name: result.data.last_name,
			    		company: result.data.company
		    		});
			    	dispatch(reset('ProfileForm'));
		    		this.toggleModal('profileModal', false);
		    	}else{
					throw new SubmissionError(Tools.errorMessageProcessing(result.message));
		    	}
		    });
	    }catch(error){
	    	console.error(error);
		}
	}

	changePasswordHandle(eventData, dispatch){
		try{
			const params = {
				password: md5(eventData.password)
			};
			if(eventData.password !== eventData.newPassword){
				return Tools.sleep().then(() => {
					throw new SubmissionError(Tools.errorMessageProcessing('Passwords not matched!'));
				});
			}
			return Tools.apiCall(apiUrls.changePassword, params).then((result) => {
		    	if(result.success){
		    		this.toggleModal('changePasswordModal', false);
		    	}else{
					throw new SubmissionError(Tools.errorMessageProcessing(result.message));
		    	}
		    });
	    }catch(error){
	    	console.error(error);
		}
	}

	_renderFullName(){
		if(this.props.authReducer.profile){
			return `${this.props.authReducer.profile.first_name || ''} ${this.props.authReducer.profile.last_name || ''}`;
		}
		return '';
	}

	_renderForAdmin(){
		return (
			<tbody>
				<tr>
					<td>Email: </td>
					<td>{this.props.authReducer.profile?this.props.authReducer.profile.email:''}</td>
				</tr>
				<tr>
					<td>Họ tên: </td>
					<td>
						{this._renderFullName()}
					</td>
				</tr>
				<tr>
					<td>Tool lấy thông tin vận đơn: </td>
					<td>
						<a href={this.state.extension_url_grab} target="_blank">
							Link
						</a>
					</td>
				</tr>
			</tbody>
		);
	}

	_renderForUser(){
		return (
			<tbody>
				<tr>
					<td>Email: </td>
					<td>{this.props.authReducer.profile?this.props.authReducer.profile.email:''}</td>
				</tr>
				<tr>
					<td>Họ tên: </td>
					<td>
						{this._renderFullName()}
					</td>
				</tr>
				<tr>
					<td>Công ty: </td>
					<td>
						{this.props.authReducer.profile.company}
					</td>
				</tr>
				<tr>
					<td>Tỷ giá: </td>
					<td>
						{this.props.authReducer.profile.rate}
					</td>
				</tr>
				<tr>
					<td>Phí đặt hàng: </td>
					<td>
						{this.props.authReducer.profile.order_fee_factor}%
					</td>
				</tr>
				<tr>
					<td>Hệ số cọc: </td>
					<td>
						{this.props.authReducer.profile.deposit_factor}%
					</td>
				</tr>
				<tr>
					<td>Hạn khiếu nại: </td>
					<td>
						{this.props.authReducer.profile.complain_day} ngày
					</td>
				</tr>
			</tbody>
		);
	}

	_renderInfo(){
		if(ADMIN_ROLES.indexOf(Tools.getStorage('authData').role) !== -1){
			return this._renderForAdmin();
		}else{
			return this._renderForUser();
		}
	}

	render() {
		return (
			<NavWrapper data-location={this.props.location} data-user={this.props.authReducer}>
				<div>
					<table className="table table-striped">
						{this._renderInfo()}
					</table>

					<div>
						<button
							type="button"
							className="btn btn-primary"
							onClick={() => this.toggleModal('profileModal')}>
							Update profile
						</button>
						<button
							type="button"
							className="btn btn-success"
							onClick={() => this.toggleModal('changePasswordModal')}>
							Change password
						</button>
					</div>

					<CustomModal
						open={this.state.profileModal}
						close={() => this.toggleModal('profileModal', false)}
						size="md"
						title="Update profile"
						>
						<div>
							<div className="custom-modal-content">
								<ProfileForm
									checkSubmit={this.updateProfileHandle.bind(this)}
									labels={labels.profile}
									submitTitle="Update profile">

									<button
										type="button"
										className="btn btn-warning cancel"
										onClick={() => this.toggleModal('profileModal', false)}>
										<span className="glyphicon glyphicon-remove"></span> &nbsp;
										Cancel
									</button>
								</ProfileForm>
							</div>

						</div>
					</CustomModal>

					<CustomModal
						open={this.state.changePasswordModal}
						close={() => this.toggleModal('changePasswordModal', false)}
						size="md"
						title="Change password"
						>
						<div>
							<div className="custom-modal-content">
								<ChangePasswordForm
									checkSubmit={this.changePasswordHandle.bind(this)}
									labels={labels.changePassword}
									submitTitle="Change password">

									<button
										type="button"
										className="btn btn-warning cancel"
										onClick={() => this.toggleModal('changePasswordModal', false)}>
										<span className="glyphicon glyphicon-remove"></span> &nbsp;
										Cancel
									</button>
								</ChangePasswordForm>
							</div>
						</div>
					</CustomModal>
				</div>
			</NavWrapper>
		);
	}
}

function mapStateToProps(state){
	return {
	}
}

function mapDispatchToProps(dispatch){
	return {
		...bindActionCreators(actionCreators, dispatch)
	};
}

Profile.propTypes = {
};

Profile.defaultProps = {
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Profile);

// export default Profile;
