import React from 'react';
import store from 'app/store';
import {ADMIN_ROLES} from 'app/constants';
import {labels} from '../_data';
import Tools from 'helpers/Tools';
import Paginator from 'utils/components/Paginator';
import {
	TableAddButton,
	TableFilter,
	TableCheckAll,
	TableCheckBox
} from 'utils/components/table/TableComponents';
import TableRow from '../components/tableRow/TableRow';


class Main1Table extends React.Component {
	static propTypes = {
	};
	static defaultProps = {
	};
	constructor(props) {
		super(props);
		this.state = {
		};
		this._renderFilter = this._renderFilter.bind(this);
		this._renderRow = this._renderRow.bind(this);
	}

	_renderFilter(){
		return (
			<TableFilter
				onFilter={this.props.onFilter}
				onRemove={this.props.onRemove}
				bulkRemove={this.props.bulkRemove}
				/>
		);
	}

	_renderRow(){
		return this.props.listItem.map((row, index) => {
			return (
				<TableRow {...this.props} key={index} row={row} index={index}/>
			);
		});
	}

	render(){
		return (
			<div>
				{this._renderFilter()}
				<table className="table table-striped">
					<thead>
						<tr>
							<th style={{width: 25}}>
								<TableCheckAll
									onCheckAll={this.props.onCheckAll}
								/>
							</th>
							<th style={{width: 60}}>Ngày</th>
							<th>Mã đơn</th>
							<th style={{width: 120}}>Tổng v.chuyển</th>
							<th style={{width: 120}}>Tổng tiền</th>
							<th>Nhân viên</th>
							<th className="center-align" style={{width: 270}}>Trạng thái</th>
							<th>Ghi chú</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						{this._renderRow()}
					</tbody>
				</table>
				<div className="pagination-wrapper">
					<Paginator
						pageCount={this.props.orderReducer.pages}
						onPageChange={this.props.onPageChange}/>
				</div>
			</div>
		);
	}
}
export default Main1Table;
