import Tools from 'helpers/Tools';
import {FIELD_TYPE} from 'app/constants';

const rawApiUrls = [
    {
        controller: 'order',
        endpoints: {
            obj: 'GET',
            list: 'GET',
            add: 'POST',
            edit: 'POST',
            remove: 'POST'
        }
    }
];

export const labels = {
	common: {
		title: 'Quản lý đơn hàng'
	},
    mainForm: {
        uid: {
            title: 'Mã',
            type: FIELD_TYPE.STRING,
            heading: true,
            init: null,
            link: {
                location: 'order',
                params: ['__type__', '__status__', '__id__']
            },
            rules: {
            }
        },
        delivery_fee: {
            title: 'Phí vận chuyển',
            type: FIELD_TYPE.FLOAT,
            heading: true,
            init: null,
            prefix: '₫',
            rules: {
            }
        },
        total: {
            title: 'Tổng tiền',
            type: FIELD_TYPE.FLOAT,
            heading: true,
            init: null,
            prefix: '₫',
            rules: {
            }
        },
        status: {
            title: 'Trạng thái',
            type: FIELD_TYPE.STRING,
            heading: true,
            init: null,
            mapLabels: 'listStatus',
            rules: {
                required: true
            }
        },
        note: {
            title: 'Ghi chú',
            type: FIELD_TYPE.STRING,
            heading: true,
            init: null,
            rules: {
            }
        },
        admin_id: {
            title: 'N.V Mua hàng',
            type: FIELD_TYPE.INTEGER,
            heading: true,
            init: null,
            mapLabels: 'listAdmin',
            rules: {
            }
        }
        , address_id: {
            title: 'Địa chỉ nhận hàng',
            type: FIELD_TYPE.INTEGER,
            init: null,
            rules: {
                required: true
            }
        }
    }
};

export const apiUrls = Tools.getApiUrlsV1(rawApiUrls);
