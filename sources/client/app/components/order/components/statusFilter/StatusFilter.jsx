import React from 'react';
import Link from 'react-router/lib/Link';
import Tools from 'helpers/Tools';


class StatusFilter extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
	    };
	    this._renderListStatus = this._renderListStatus.bind(this);
	}

	_renderListStatus(){
		return this.props.listStatus.map((status, index) => {
			return(
				<Link
					activeClassName="active"
					to={Tools.toUrl('order', [this.props.type, status.id])}
					className="btn btn-default" key={index}>
					{status.title} &rarr; {status.total}
				</Link>
			);
		});
	}

	render() {
		return (
			<div className="btn-group btn-group-justified" role="group">
				<Link
					activeClassName="active"
					to={Tools.toUrl('order', [this.props.type, 'all'])}
					className="btn btn-default">Tất cả</Link>
				{this._renderListStatus()}
			</div>
		);
	}
}

StatusFilter.propTypes = {
	type: React.PropTypes.string.isRequired,
	listStatus: React.PropTypes.array.isRequired
};

StatusFilter.defaultProps = {
	listStatus: []
};

export default StatusFilter;
