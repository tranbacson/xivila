import Tools from 'helpers/Tools';
import {FIELD_TYPE} from 'app/constants';

const rawApiUrls = [
    {
        controller: 'order',
        endpoints: {
            addFull: 'POST',
            uploadCart: 'POST'
        }
    }
];

export const labels = {
	common: {
		title: 'Quản lý cart'
	},
    mainForm: {
        quantity: {
            title: 'Số lượng',
            type: FIELD_TYPE.INTEGER,
            heading: true,
            init: 0,
            rules: {
                required: true
            }
        },
        properties: {
            title: 'Thuộc tính',
            type: FIELD_TYPE.STRING,
            heading: true,
            init: 0,
            rules: {
            }
        },
        message: {
            title: 'Ghi chú',
            type: FIELD_TYPE.STRING,
            heading: false,
            init: null,
            rules: {
            }
        }
    },
    createOrderForm: {
        title: {
            title: 'Tên đơn hàng',
            type: FIELD_TYPE.STRING,
            heading: false,
            init: null,
            rules: {
                required: true,
                min: 3
            }
        },
        payment_method: {
            payment_method: 'Hình thức thanh toán',
            type: FIELD_TYPE.STRING,
            heading: false,
            init: 'direct',
            rules: {
            }
        }
    }
};

export const apiUrls = Tools.getApiUrlsV1(rawApiUrls);
