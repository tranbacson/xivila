import React from 'react';
import values from 'lodash/values';
import forEach from 'lodash/forEach';
import {labels} from './_data';
import Tools from 'helpers/Tools';
import NavWrapper from 'utils/components/NavWrapper';
import CustomModal from 'utils/components/CustomModal';
import MainTable from './tables/Main.table';
import MainForm from './forms/Main.form';
import UploadForm from './forms/Upload.form';

class CartLayout extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return (
			<div>
				<div className="main-content">
					<UploadForm
						onSubmit={this.props.onUpload}
						labels={labels.mainForm}
						submitTitle="Upload"/>
					<MainTable {...this.props}/>
				</div>
				<CustomModal
					open={this.props.mainModal}
					close={() => this.props.toggleModal(null, 'mainModal', false)}
					size="md"
					title="Cart manager"
					>
					<div>
						<div className="custom-modal-content">
							<MainForm
								onSubmit={this.props.onChange}
								labels={labels.mainForm}
								dataReducer="cartReducer"
								dataTarget="obj"
								submitTitle="Save">

								<button
									type="button"
									className="btn btn-warning cancel"
									onClick={() => this.props.toggleModal(null, 'mainModal', false)}>
									<span className="glyphicon glyphicon-remove"></span> &nbsp;
									Cancel
								</button>
							</MainForm>
						</div>
					</div>
				</CustomModal>
			</div>
		);
	}
}

CartLayout.propTypes = {
	bulkRemove: React.PropTypes.bool
};

CartLayout.defaultProps = {
	bulkRemove: true
};

export default CartLayout;
