import React from 'react';
import isEmpty from 'lodash/isEmpty';
import forEach from 'lodash/forEach';
import keys from 'lodash/keys';
import values from 'lodash/values';
import filter from 'lodash/filter';
import find from 'lodash/find';
import findIndex from 'lodash/findIndex';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { SubmissionError, reset } from 'redux-form';
import * as actionCreators from 'app/actions/actionCreators';
import store from 'app/store';
import { BASE_URL } from 'app/constants';
import {apiUrls, labels} from './_data';
import Tools from 'helpers/Tools';
import CartLayout from './Cart.layout';

class Cart extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			mainModal: false,
			itemId: null,
			bulkRemove: true,
			params: {}
	    };
	    this.list = this.list.bind(this);
	    this.toggleModal = this.toggleModal.bind(this);
	    this.handleFilter = this.handleFilter.bind(this);
	    this.handleCheck = this.handleCheck.bind(this);
	    this.handleCheckAll = this.handleCheckAll.bind(this);
	    this.handleCheckAllShop = this.handleCheckAllShop.bind(this);
	    this.handleRemove = this.handleRemove.bind(this);
	    this.handleChange = this.handleChange.bind(this);
	    this.handlePageChange = this.handlePageChange.bind(this);
	    this.handleAddOrder = this.handleAddOrder.bind(this);
	    this.handleUpload = this.handleUpload.bind(this);
	    this.setInitData = this.setInitData.bind(this);
	    this.filterTimeout = null;
	}

	componentDidMount(){
		document.title = 'Cart';
		this.setInitData();
	}

	setInitData(){
		let listItemStorage = Tools.getStorage('orderItems');
		let listItem = Tools.orderItemsParse(listItemStorage);
		this.props.cartAction('newList', {list: listItem, pages: 1});
	}

	toggleModal(id=null, state, open=true){
		let newState = {};
		newState[state] = open;

		switch(state){
			case 'mainModal':
				newState.itemId = id;
				if(id && open){
					let listItemStorage = Tools.getStorage('orderItems');
					let item = find(listItemStorage, {id: id});
					if(item){
						this.props.cartAction('obj', {
							...item
						});
						this.setState(newState);
						return;
					}
				}else{
					this.props.cartAction('obj', Tools.getInitData(labels.mainForm));
					this.setState(newState);
				}
			break;
		}
	}

	list(outerParams={}, page=1){
	    let params = {
	    	page
	    };

		if(!isEmpty(outerParams)){
	    	params = {...params, ...outerParams};
	    }

		Tools.apiCall(apiUrls.list, params, false).then((result) => {
	    	if(result.success){
		    	this.props.cartAction('newList', {list: result.data.items, pages: result.data._meta.last_page});
	    	}
	    });
	}

	handleFilter(event){
		let keyword = event.target.value;
		if(this.filterTimeout !== null){
			clearTimeout(this.filterTimeout);
		}
		this.filterTimeout = setTimeout(() => {
			if(keyword.length > 2){
				this.list({keyword: keyword});
			}else if(!keyword.length){
				this.list();
			}
		}, 600);
	}

	handlePageChange(data){
		let page = data.selected + 1;
		this.list({}, page);
	}

	totalCalculating(){
		let totalSelected = 0;
		let totalAll = 0;
		let rate = 0;
		forEach(this.props.cartReducer.list.shops, shop => {
			forEach(shop.items, item => {
				totalAll += item.quantity * parseFloat(item.unit_price);
				if(item.rate && !rate){
					rate = item.rate
				}
				if(item.checked){
					totalSelected += item.quantity * parseFloat(item.unit_price);
				}
			});
		});
		if(!totalSelected){
			totalSelected = totalAll;
		}
		return {
			totalSelected,
			totalSelectedWithRate: totalSelected * rate
		}
	}

	handleCheck(id, shopIndex, checked){
		let totalSelected = 0;
		let totalAll = 0;
		let rate = 0;
		let index = store.getState().cartReducer.list.shops[shopIndex].items.findIndex(x => x.id===id);
		this.props.cartAction('edit', {checked}, shopIndex, index); // Mark checked
		setTimeout(() => {
			this.props.cartAction('getTotalWhenChecked', this.totalCalculating());
		}, 100);
	}

	handleCheckAll(){
		let list = this.props.cartReducer.list;
		let totalCheck = 0;
		let totalItem = 0;
		forEach(this.props.cartReducer.list.shops, shop => {
			totalItem += shop.items.length;
			forEach(shop.items, item => {
				if(item.checked){
					totalCheck++;
				}
			});
		});

		if(totalCheck === totalItem){
			this.props.cartAction('uncheckAll');
		}else{
			this.props.cartAction('checkAll');
		}

		setTimeout(() => {
			this.props.cartAction('getTotalWhenChecked', this.totalCalculating());
		}, 100);
	}

	handleCheckAllShop(shopIndex){
		let list = this.props.cartReducer.list;
		let totalCheck = 0;
		let totalItem = this.props.cartReducer.list.shops[shopIndex].items.length;
		forEach(this.props.cartReducer.list.shops[shopIndex].items, item => {
			if(item.checked){
				totalCheck++;
			}
		});

		if(totalCheck === totalItem){
			this.props.cartAction('uncheckAllShop', null, shopIndex);
		}else{
			this.props.cartAction('checkAllShop', null, shopIndex);
		}

		setTimeout(() => {
			this.props.cartAction('getTotalWhenChecked', this.totalCalculating());
		}, 100);
	}

	handleRemove(id){
		const confirm = window.confirm('Do you want to remove this item(s)?');
		if(!confirm){
			return;
		}
		let listItemStorage = Tools.getStorage('orderItems');
		let index = findIndex(listItemStorage, {id: id});
		listItemStorage = [
			...listItemStorage.slice(0, index),
			...listItemStorage.slice(index + 1)
		];
		Tools.setStorage('orderItems', listItemStorage);
		this.props.cartAction('newList', {list: Tools.orderItemsParse(listItemStorage), pages: 1});
	}

	handleChange(eventData, dispatch){
		try{
			const params = {...eventData};
			const id = this.state.itemId;

			let listItemStorage = Tools.getStorage('orderItems');
			let index = findIndex(listItemStorage, {id: id});
			listItemStorage = [
				...listItemStorage.slice(0, index),
				{...listItemStorage.slice(index, index + 1)[0], ...params},
				...listItemStorage.slice(index + 1)
			];
			Tools.setStorage('orderItems', listItemStorage);

			this.props.cartAction('newList', {list: Tools.orderItemsParse(listItemStorage), pages: 1});

			dispatch(reset('CardMainForm'));
		    this.toggleModal(null, 'mainModal', false);
	    }catch(error){
	    	console.error(error);
			throw new SubmissionError(Tools.errorMessageProcessing(error));
		}
	}

	handleAddOrder(){
		if(!Tools.getStorage('authData')){
			alert('Bạn vui lòng đăng nhập trước khi tạo đơn hàng.');
			return;
		}

		let listItem = [];
		let listRemain = [];
		forEach(this.props.cartReducer.list.shops, shop => {
			forEach(shop.items, item => {
				if(item.checked){
					listItem.push(item);
				}else{
					listRemain.push(item);
				}
			});
		});

		if(!listItem.length){
			const confirm = window.confirm('Bạn chưa chọn sản phẩm nào trong giỏ hàng. Bạn có muốn đưa toàn bộ sản phẩm trong giỏ hàng vào đơn hàng không?');
			if(confirm){
				console.log('hura');
				listItem = [...listRemain];
				listRemain = [];
			}
		}
		const params = {
			listOrderItem: JSON.stringify(listItem)
		};
		Tools.apiCall(apiUrls.addFull, params).then((result) => {
	    	if(result.success){

				Tools.setStorage('orderItems', listRemain);
				this.props.cartAction('newList', {list: listRemain, pages: 1});
				setTimeout(()=>{
		    		window.top.location = BASE_URL + 'order/normal/new/' + result.data.id;
				}, 200);

	    	}
	    });
	}

	handleUpload(eventData, dispatch){
		Tools.apiCall(apiUrls.uploadCart, eventData).then((result) => {
	    	if(result.success){
	    		const listNewItem = result.data.items;
	    		let listItem = Tools.getStorage('orderItems');

				forEach(listNewItem, function(value){
			    	var matchIndex = findIndex(listItem, {url: value.url, properties: value.properties});
			    	if(matchIndex !== -1){
			    		// Match -> Increase quantity
			    		listItem[matchIndex].quantity = parseInt(listItem[matchIndex].quantity) + parseInt(value.quantity);
			    	}else{
			    		// Not Match -> Insert
			    		listItem.push(value);
			    	}
			    });
			    Tools.setStorage('orderItems', listItem);
			    setTimeout(() => this.setInitData(), 50);
		    	// window.localStorage[mainKey] = JSON.stringify(listItem);
	    	}
	    });
	}

	render() {
		return (
			<CartLayout
				{...this.props}
				mainModal={this.state.mainModal}
				bulkRemove={this.state.bulkRemove}
				list={this.list}
				toggleModal={this.toggleModal}
				onFilter={this.handleFilter}
				onCheck={this.handleCheck}
				onCheckAll={this.handleCheckAll}
				onCheckAllShop={this.handleCheckAllShop}
				onRemove={this.handleRemove}
				onChange={this.handleChange}
				onUpload={this.handleUpload}
				onPageChange={this.handlePageChange}
				onAddOrder={this.handleAddOrder}
				/>
		);
	}
}

function mapStateToProps(state){
	return {
	}
}

function mapDispatchToProps(dispatch){
	return {
		...bindActionCreators(actionCreators, dispatch),
		resetForm: (formName) => {
			dispatch(reset(formName));
		}
	};
}

Cart.propTypes = {
};

Cart.defaultProps = {
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Cart);

// export default Cart;
