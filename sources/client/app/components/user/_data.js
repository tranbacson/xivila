import Tools from 'helpers/Tools';
import {FIELD_TYPE} from 'app/constants';
import store from 'app/store';

const rawApiUrls = [
    {
        controller: 'user',
        endpoints: {
            obj: 'GET',
            list: 'GET',
            add: 'POST',
            edit: 'POST',
            remove: 'POST',
            signup: 'POST'
        }
    }, {
        controller: 'areaCode',
        endpoints: {
            list: 'GET'
        }
    }
];

export const labels = {
	common: {
		title: 'Quản lý user'
	},
    mainForm: {
        id: {
            title: 'Mã',
            type: FIELD_TYPE.INTEGER,
            heading: true,
            init: null,
            width: 50,
            rules: {
            }
        },
        admin_id: {
            title: 'Nhân viên chăm sóc',
            type: FIELD_TYPE.INTEGER,
            heading: true,
            mapLabels: 'listAdmin',
            init: null,
            width: 150,
            rules: {
            }
        },
        full_name: {
            title: 'Họ tên',
            type: FIELD_TYPE.STRING,
            heading: true,
            init: null,
            width: 150,
            rules: {
            }
        },
        first_name: {
            title: 'Tên',
            type: FIELD_TYPE.STRING,
            heading: false,
            init: null,
            width: 100,
            rules: {
                required: true
            }
        }, last_name: {
            title: 'Họ',
            type: FIELD_TYPE.STRING,
            heading: false,
            init: null,
            width: 100,
            rules: {
                required: true
            }
        }, email: {
            title: 'Email',
            type: FIELD_TYPE.EMAIL,
            heading: true,
            init: null,
            width: 200,
            rules: {
                required: true
            }
        }, phone: {
            title: 'Số điện thoại',
            type: FIELD_TYPE.STRING,
            heading: true,
            init: null,
            width: 100,
            rules: {
                required: true
            }
        }, area_code_id: {
            title: 'Vùng',
            type: FIELD_TYPE.INTEGER,
            heading: true,
            init: null,
            mapLabels: 'listAreaCode',
            width: 150,
            rules: {
                required: true
            }
        }, address: {
            title: 'Địa chỉ',
            type: FIELD_TYPE.STRING,
            heading: true,
            init: null,
            width: 220,
            rules: {
                required: true
            }
        }, company: {
            title: 'Công ty',
            type: FIELD_TYPE.STRING,
            heading: true,
            init: null,
            width: 100,
            rules: {
            }
        }, order_fee_factor: {
            title: 'Phí đặt hàng',
            type: FIELD_TYPE.FLOAT,
            heading: true,
            init: 5,
            width: 100,
            suffix: '%',
            rules: {
                required: true,
                min: 0,
                max: 100
            }
        }, rate: {
            title: 'Tỷ giá',
            type: FIELD_TYPE.INTEGER,
            heading: true,
            init: 3400,
            width: 60,
            rules: {
                required: true
            }
        }, deposit_factor: {
            title: 'Hệ số cọc',
            type: FIELD_TYPE.FLOAT,
            heading: true,
            init: 50,
            width: 100,
            suffix: '%',
            rules: {
                required: true,
                min: 0,
                max: 100
            }
        }, complain_day: {
            title: 'Hạn kh.nại',
            type: FIELD_TYPE.INTEGER,
            heading: true,
            init: 2,
            width: 100,
            suffix: 'ngày',
            rules: {
                required: true,
                min: 1,
                max: 10
            }
        }, password: {
            title: 'Mật khẩu',
            type: FIELD_TYPE.STRING,
            init: null,
            rules: {
                required: true,
                min: 6,
                max: 30
            }
        }
    }
};

export const apiUrls = Tools.getApiUrlsV1(rawApiUrls);
