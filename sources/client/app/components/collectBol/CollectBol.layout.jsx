import React from 'react';
import values from 'lodash/values';
import forEach from 'lodash/forEach';
import Table from 'rc-table';
import {labels} from './_data';
import Tools from 'helpers/Tools';
import NavWrapper from 'utils/components/NavWrapper';
import CustomModal from 'utils/components/CustomModal';
import MainTable from './tables/Main.table';
import MainForm from './forms/Main.form';
import WaitingMessage from 'utils/components/WaitingMessage';

import Select from 'react-select';
import moment from 'moment';
import 'react-dates/lib/css/_datepicker.css';
import { DateRangePicker } from 'react-dates';


class CollectBolLayout extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			startDate: null,
			endDate: null,
			focusedInput: null,
			adminId: 0
		};
	}

	_renderContent(){
		if(!this.props.dataLoaded){
			return <WaitingMessage/>
		}
		return (
			<div>
				<div className="breadcrumb-container">
					{labels.common.title}
				</div>
				<div className="main-content">
					<table>
						<tbody>
							<tr>
								<td>
									<DateRangePicker
										startDate={this.state.startDate}
										endDate={this.state.endDate}
										onDatesChange={({ startDate, endDate }) => {
											if(!startDate && !endDate){
												this.setState({ startDate, endDate })
												this.props.setFilterDateParam(startDate, endDate);
											}else{
												const startDateFormat = startDate.format('YYYY/MM/DD');
												if(endDate){
													const endDateFormat = endDate.format('YYYY/MM/DD');
													this.setState({ startDate, endDate })
													this.props.setFilterDateParam(startDateFormat, endDateFormat);
												}else{
													this.setState({ startDate, endDate: startDate });
													this.props.setFilterDateParam(startDateFormat, startDateFormat);
												}
											}
										}}
										focusedInput={this.state.focusedInput}
										onFocusChange={focusedInput => this.setState({ focusedInput })}
										startDatePlaceholderText="Từ ngày"
										endDatePlaceholderText="Đến ngày"
										monthFormat="MM / YYYY"
										displayFormat="DD/MM/YYYY"
										showClearDates={true}
										isOutsideRange={day => false}
										isDayHighlighted={day => (day.isSame(moment(), 'day')?true:false)}
									/>
								</td>
								<td width="250px">
									<Select
										name="admin_id"
										valueKey="id"
										labelKey="title"
										value={this.state.adminId}
										options={this.props.collectBolReducer.listAdmin}
										onChange={value => {this.props.setFilterAdminParam(value.id);this.setState({adminId: value.id})}}/>
								</td>
							</tr>
						</tbody>
					</table>

					<MainTable {...this.props}/>
				</div>

				<CustomModal
					open={this.props.mainModal}
					close={() => this.props.toggleModal(null, 'mainModal', false)}
					size="md"
					title="CollectBol manager"
					>
					<div>
						<div className="custom-modal-content">
							<MainForm
								onSubmit={this.props.onChange}
								labels={labels.mainForm}
								submitTitle="Save">

								<button
									type="button"
									className="btn btn-warning cancel"
									onClick={() => this.props.toggleModal(null, 'mainModal', false)}>
									<span className="glyphicon glyphicon-remove"></span> &nbsp;
									Cancel
								</button>
							</MainForm>
						</div>
					</div>
				</CustomModal>
			</div>
		);
	}

	render() {
		return (
			<NavWrapper data-location={this.props.location} data-user={this.props.authReducer}>
				<div>
					{this._renderContent()}
				</div>
			</NavWrapper>
		);
	}
}

CollectBolLayout.propTypes = {
	bulkRemove: React.PropTypes.bool
};

CollectBolLayout.defaultProps = {
	bulkRemove: true
};

export default CollectBolLayout;
