import { call, take, put, select } from 'redux-saga/effects';
import { SubmissionError, reset, startSubmit, stopSubmit } from 'redux-form';
import {apiUrls} from '../_data';
import Tools from 'helpers/Tools';
import ConfigApi from '../Config.api';


export default function* toggleModalSaga() {
	while(true){
		const {payload} = yield take('config/toggleModal');
		if(payload.open){
			// Cleaning data
			yield put({type: 'config/obj', payload: {}});
			// Reset form status
			yield put(reset('ConfigMainForm'));
		}else{
			// Forget itemId when close modal
			yield put({type: 'config/itemId', payload: {id: null}});
		}
	}
}