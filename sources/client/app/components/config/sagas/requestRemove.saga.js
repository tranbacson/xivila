import { call, take, put, select } from 'redux-saga/effects';
import { SubmissionError, reset, startSubmit, stopSubmit } from 'redux-form';
import {apiUrls} from '../_data';
import Tools from 'helpers/Tools';
import ConfigApi from '../Config.api';


export default function* requestRemoveSaga(){
	while(true){
		const {payload: {id}}  = yield take('config/requestRemove');

		const {configReducer: {listLoaded}} = yield select();
		if(!listLoaded){
			yield put({type: 'config/listLoaded', payload: {loaded: false}});
		}
		yield call(requestList, params, page);
	}
}

function* requestRemove(){

}