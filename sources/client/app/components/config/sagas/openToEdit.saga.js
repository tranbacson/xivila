import { call, take, put, select } from 'redux-saga/effects';
import { SubmissionError, reset, startSubmit, stopSubmit } from 'redux-form';
import {apiUrls} from '../_data';
import Tools from 'helpers/Tools';
import ConfigApi from '../Config.api';


export default function* openToEditSaga() {
	while(true){
		const {payload} = yield take('config/openToEdit');
		yield call(openToEdit, payload.id);
	}
}

function* openToEdit(id){
	// Get data
	const result = yield call(
		Tools.apiCall.bind(Tools),
		apiUrls.obj,
		{id},
		false
	);
	if(result.success){
		// Remember itemId
		yield put({type: 'config/itemId', payload: {id: result.data.id}});
		// Set data
		yield put({type: 'config/toggleModal', payload: {modalId: 'mainModal', open: true}});
		// Toggle modal
		yield put({type: 'config/obj', payload: result.data});
	}else{
		console.error(result.message);
	}
}