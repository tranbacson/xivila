import { call, take, put, select } from 'redux-saga/effects';
import { SubmissionError, reset, startSubmit, stopSubmit } from 'redux-form';
import {apiUrls} from '../_data';
import Tools from 'helpers/Tools';
import ConfigApi from '../Config.api';


export default function* submitMainFormSaga(data){
	while(true){
		const {payload: params} = yield take('config/submitMainForm');
		const {configReducer: {itemId: id}} = yield select();
		yield call(submitMainForm, id, params);
	}
}

function* submitMainForm(id, params){
	try{
		const result = yield call(Tools.apiCall.bind(Tools), apiUrls[id?'edit':'add'], id?{...params, id}:params);
		if(result.success){
			const data = {...result.data};
			if(id){
				const {configReducer} = yield select();
				const index = configReducer.list.findIndex(x => x.id===id);
				yield put({type: 'config/edit', payload: {data, index}});
			}else{
				yield put({type: 'config/add', payload: {data}});
			}
			yield put({type: 'config/toggleModal', payload: {modalId: 'mainModal', open: false}});
		}else{
			yield put(stopSubmit('ConfigMainForm', Tools.errorMessageProcessing(result.message)));
		}
	}catch(error){
		yield put(stopSubmit('ConfigMainForm', Tools.errorMessageProcessing(error)));
	}
}