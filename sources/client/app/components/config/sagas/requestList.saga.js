import { call, take, put, select } from 'redux-saga/effects';
import { SubmissionError, reset, startSubmit, stopSubmit } from 'redux-form';
import {apiUrls} from '../_data';
import Tools from 'helpers/Tools';
import ConfigApi from '../Config.api';

export default function* requestListSaga(){
	while(true){
		const {payload: {params, page}}  = yield take('config/requestList');
		const {configReducer: {listLoaded}} = yield select();
		if(!listLoaded){
			yield put({type: 'config/listLoaded', payload: {loaded: false}});
		}
		yield call(requestList, params, page);
	}
}

function* requestList(params, page){
	console.log(params, page);
	const result = yield call(ConfigApi.list, params, page);
	const {configReducer: {listLoaded}} = yield select();
	if(!listLoaded){
		yield put({type: 'config/listLoaded', payload: {loaded: true}});
	}
	if(result.success){
		yield put({type: 'config/list', payload: {data: result.data.items, pages: result.data._meta.last_page}});
	}
}
