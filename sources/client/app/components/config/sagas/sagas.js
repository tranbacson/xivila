import requestListSaga from './requestList.saga';
import toggleModalSaga from './toggleModal.saga';
import openToEditSaga from './openToEdit.saga';
import submitMainFormSaga from './submitMainForm.saga';

export default function* configSaga() {
	yield [
		requestListSaga(),
		toggleModalSaga(),
		openToEditSaga(),
		submitMainFormSaga()
	];
}