import isEmpty from 'lodash/isEmpty';

import store from 'app/store';
import * as actions from 'app/actions/actionCreators';
import {apiUrls} from './_data';
import Tools from 'helpers/Tools';


export default class ConfigApi {
	static setInitData(initData){
		store.dispatch(
			actions.configAction(
				'newList',
				{
					list: [...initData.data.items],
					pages: initData.data._meta.last_page
				}
			)
		);
		// this.setState({dataLoaded: true});
	}

	static list(outerParams={}, page=1){
	    let params = {
	    	page
	    };

		if(!isEmpty(outerParams)){
	    	params = {...params, ...outerParams};
	    }
		return Tools.apiCall(apiUrls.list, params, false);
	}
}
