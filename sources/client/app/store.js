import { createStore, compose, applyMiddleware } from 'redux';
import { syncHistoryWithStore, routerMiddleware } from 'react-router-redux';
import browserHistory from 'react-router/lib/browserHistory';


// Import the root reducer
import rootReducer from './reducers/index';
import rootSaga from './saga';

// Create an object for the default data
import createSagaMiddleware from 'redux-saga';
const sagaMiddleware = createSagaMiddleware();


// Create an object for the default data
const middlewares = [routerMiddleware(browserHistory), sagaMiddleware];
const listStatus = [
	{id: 'new', title: 'Chờ duyệt'},
	{id: 'confirm', title: 'Duyệt mua'},
	{id: 'purchasing', title: 'Đang giao dịch'},
	{id: 'complain', title: 'Khiếu nại'},
	{id: 'done', title: 'Hoàn Thành'}
];
const currentYear = new Date().getFullYear();
const currentMonth = new Date().getMonth();
const defaultState = {
	commonReducer: {
		listRegion: [
			{id: 'HN', title: 'HN: Hà Nội'},
			{id: 'HNL', title: 'HNL: Lân cận Hà Nội'},
			{id: 'SG', title: 'SG: Sài Gòn'},
			{id: 'SGL', title: 'SGL: Lân cận Sài Gòn'},
			{id: 'DN', title: 'DN: Đà Nẵng'},
			{id: 'DNL', title: 'DNL: Lân cận Đà Nẵng'},
			{id: 'LK', title: 'LK: Lưu kho TQ'}
		]
	},
	wrapperReducer:{
		firstUpdate: true
	},
	authReducer: {
		login: {
			email: null,
			password: null
		}, profile: {
			email: null,
			first_name: null,
			last_name: null,
			role_id: null
		}
	},
	adminReducer: {
		list: [],
		obj: {},
		pages: 1,
		listRole: [],
		defaultRole: []
	},
	userReducer: {
		list: [],
		obj: {},
		pages: 1,
		listRegion: [],
		defaultRegion: null,
		listAdmin: [],
		defaultAdmin: null,
		listAreaCode: [],
		defaultAreaCode: null
	},
	configReducer: {
		list: [],
		obj: {},
		pages: 1
	},
	permissionReducer: {
		list: [],
		obj: {},
		pages: 1
	},
	roleTypeReducer: {
		list: [],
		obj: {},
		pages: 1
	},
	roleReducer: {
		list: [],
		obj: {},
		pages: 1
	},
	categoryReducer: {
		list: [],
		obj: {},
		pages: 1,
		listType: [
			{id: 'article', title: 'Bài viết'},
		    {id: 'banner', title: 'Banner'}
		]
	},
	bannerReducer: {
		list: [],
		obj: {},
		pages: 1
	},
	articleReducer: {
		list: [],
		obj: {},
		pages: 1
	},
	addressReducer: {
		list: {},
		obj: {},
		pages: 1,
		listAreaCode: [],
		defaultAreaCode: null
	},
	bankReducer: {
		list: [],
		obj: {},
		pages: 1,
		listType: [
			{id: 'vn', title: 'Việt Nam'},
		    {id: 'cn', title: 'Trung Quốc'}
		]
	},
	cartReducer: {
		list: {},
		obj: {},
		pages: 1,
		totalSelected: 0,
		totalSelectedWithRate: 0
	},
	grabbingReducer: {
		list: {},
		obj: {},
		pages: 1
	},
	shopReducer: {
		list: [],
		obj: {},
		pages: 1
	},
	orderReducer: {
		list: [],
		listAddress: [],
		defaultAddress: null,
		listAdmin: [],
		defaultAdmin: null,
		obj: {},
		pages: 1,
		listStatus
	},
	orderDetailReducer: {
		list: [],
		listBillOfLanding: [],
		obj: {},
		pages: 1,
		objItem: {},
		objPurchase: {},
		objBillOfLanding: {},
		selectedShop: null,
		listStatus,
		objStatus: {},
		listAddress: [],
		objAddress: {},
		listAdmin: [],
		defaultAdmin: null
	},
	billOfLandingReducer: {
		list: [],
		obj: {},
		pages: 1,
		listAddress: [],
		defaultAddress: null,
		listLandingStatus: [
			{id: 'Mới', title: 'Mới'},
			{id: 'Về TQ', title: 'Về TQ'},
			{id: 'Về VN', title: 'Về VN'},
			{id: 'Đã xuất', title: 'Đã xuất'}
		],
		listLandingStatusFilter: [
			{id: 'all', title: 'Tất cả'},
			{id: 'new', title: 'Mới'},
			{id: 'cn', title: 'Về TQ'},
			{id: 'vn', title: 'Về VN'},
			{id: 'export', title: 'Đã xuất'},
			{id: 'complain', title: 'Khiếu nại'}
		]
	},
	cnBillOfLandingReducer: {
		list: [],
		obj: {},
		pages: 1
	},
	vnBillOfLandingReducer: {
		list: [],
		obj: {},
		pages: 1
	},
	exportBillReducer: {
		list: [],
		obj: {},
		pages: 1
	},
	exportBillDetailReducer: {
		listSelected: [],
		listPure: [],
		listFail: [],
		obj: {},
		keyword: '',
		address_uid: '',
		pages: 1
	},
	exportBillPreviewReducer: {
		obj: {}
	},
	bolReportReducer: {
		list: [],
		listYear: [0, 1, 2, 3].map(deltaYear => {
			return {id: currentYear - deltaYear, title: currentYear - deltaYear}
		}),
		listMonth: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(month => {
			return {id: month, title: "Tháng " + month}
		}),
		initFilter: {selected_year: currentYear, selected_month: currentMonth + 1}
	},
	rateLogReducer: {
		list: [],
		obj: {},
		pages: 1
	},
	cnBillOfLandingFailReducer: {
		list: [],
		obj: {},
		pages: 1
	},
	userOrderLogReducer: {
		list: [],
		pages: 1
	},
	areaCodeReducer: {
		list: [],
		obj: {},
		pages: 1
	},
	checkBillReducer: {
		list: [],
		listPurchase: [],
		obj: {},
		keyword: '',
		pages: 1
	},
	collectBolReducer: {
		list: [],
		obj: {},
		pages: 1,
		listAdmin: []
	},
	spinner: {
		show: false
	}
};
export const intState = {...defaultState};

const store = createStore(rootReducer, defaultState, applyMiddleware(...middlewares));

sagaMiddleware.run(rootSaga);

export const history = syncHistoryWithStore(browserHistory, store);

export const action = actionCreator => store.dispatch(actionCreator);
export const actionPure = type => store.dispatch({type});
export default store;