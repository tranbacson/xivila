import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import {reducer as toastrReducer} from 'react-redux-toastr';
import { reducer as formReducer } from 'redux-form'

import commonReducer from './commonReducer';
import wrapperReducer from './wrapperReducer';
import authReducer from './authReducer';
import adminReducer from './adminReducer';
import userReducer from './userReducer';
import configReducer from './configReducer';
import spinner from './spinner';

const rootReducer = combineReducers({
	commonReducer,
	wrapperReducer,
	authReducer,
	adminReducer,
	userReducer,
	configReducer,
	spinner,
	form: formReducer,
	routing: routerReducer,
	toastr: toastrReducer
});

export default rootReducer;