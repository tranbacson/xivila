export function updateProfile(data){
	return { type: 'AUTH_UPDATE_PROFILE', data }
}

export function logout(data){
	return { type: 'AUTH_LOGOUT', data }
}

export function wrapperAction(action){
	const PREFIX = 'WRAPPER' + '_';
	switch(action){
		case 'update':
			return { type: PREFIX + 'UPDATE'};
	}
}

export function configAction(action, data=[], index=null){
	const PREFIX = 'CONFIG' + '_';
	switch(action){
		case 'obj':
			return { type: PREFIX + 'OBJ', data };
		case 'newList':
			return { type: PREFIX + 'NEW_LIST', data };
		case 'appendList':
			return { type: PREFIX + 'APPEND_LIST', data };
		case 'add':
			return { type: PREFIX + 'ADD_ITEM', data };
		case 'edit':
			return { type: PREFIX + 'EDIT_ITEM', data, index: index };
		case 'remove':
			return { type: PREFIX + 'REMOVE_ITEM', data, listIndex: index };
		case 'checkAll':
			return { type: PREFIX + 'CHECK_ALL'};
		case 'uncheckAll':
			return { type: PREFIX + 'UNCHECK_ALL'};
	}
}
export function adminAction(action, data=[], index=null){
	const PREFIX = 'ADMIN' + '_';
	switch(action){
		case 'obj':
			return { type: PREFIX + 'OBJ', data };
		case 'newList':
			return { type: PREFIX + 'NEW_LIST', data };
		case 'appendList':
			return { type: PREFIX + 'APPEND_LIST', data };
		case 'add':
			return { type: PREFIX + 'ADD_ITEM', data };
		case 'edit':
			return { type: PREFIX + 'EDIT_ITEM', data, index: index };
		case 'remove':
			return { type: PREFIX + 'REMOVE_ITEM', data, listIndex: index };
		case 'checkAll':
			return { type: PREFIX + 'CHECK_ALL'};
		case 'uncheckAll':
			return { type: PREFIX + 'UNCHECK_ALL'};
		case 'setListRole':
			return { type: PREFIX + 'SET_LIST_ROLE', data };
		case 'setDefaultRole':
			return { type: PREFIX + 'SET_DEFAULT_ROLE', data };
	}
}
export function userAction(action, data=[], index=null){
	const PREFIX = 'USER' + '_';
	switch(action){
		case 'obj':
			return { type: PREFIX + 'OBJ', data };
		case 'newList':
			return { type: PREFIX + 'NEW_LIST', data };
		case 'appendList':
			return { type: PREFIX + 'APPEND_LIST', data };
		case 'listAdmin':
			return { type: PREFIX + 'LIST_ADMIN', data };
		case 'defaultAdmin':
			return { type: PREFIX + 'DEFAULT_ADMIN', data };
		case 'add':
			return { type: PREFIX + 'ADD_ITEM', data };
		case 'edit':
			return { type: PREFIX + 'EDIT_ITEM', data, index: index };
		case 'remove':
			return { type: PREFIX + 'REMOVE_ITEM', data, listIndex: index };
		case 'checkAll':
			return { type: PREFIX + 'CHECK_ALL'};
		case 'uncheckAll':
			return { type: PREFIX + 'UNCHECK_ALL'};
		case 'setListRegion':
			return { type: PREFIX + 'SET_LIST_REGION', data };
		case 'setDefaultRegion':
			return { type: PREFIX + 'SET_DEFAULT_REGION', data };
		case 'listAreaCode':
			return { type: PREFIX + 'LIST_AREA_CODE', data };
		case 'defaultAreaCode':
			return { type: PREFIX + 'DEFAULT_AREA_CODE', data };
	}
}
