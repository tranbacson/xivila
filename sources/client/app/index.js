import {URL_PREFIX, APP} from 'app/constants';

import 'libs/bootstrap/css/bootstrap.min.css';

import 'react-select/dist/react-select.min.css';
import 'react-redux-toastr/src/styles/index.scss';
import 'react-dd-menu/dist/react-dd-menu.min.css';
import 'rc-table/assets/index.css';
import './styles/main.styl';

import React from 'react';
import ReactDOM from 'react-dom';

import Router from 'react-router/lib/Router';
import Route from 'react-router/lib/Route';
import Redirect from 'react-router/lib/Redirect';
import IndexRoute from 'react-router/lib/IndexRoute';
import browserHistory from 'react-router/lib/browserHistory';


import { Provider } from 'react-redux';
import ReduxToastr from 'react-redux-toastr';

import store, { history } from './store';
import Tools from 'helpers/Tools';

/* COMMON COMPONENTS*/
import App from 'components/App';
import NotFound from 'utils/components/404';

/* AUTH COMPONENTS*/
import Login from 'components/auth/Login';
import Profile from 'components/auth/Profile';
import PasswordConfirm from 'components/auth/PasswordConfirm';

/* Admin COMPONENTS*/
import Admin from 'components/admin/Admin';
/* User COMPONENTS*/
import User from 'components/user/User';
import Signup from 'components/user/components/signup/Signup';
/* CONFIG COMPONENTS*/
import Config from 'components/config/Config';


/* HOME COMPONENTS*/
import Home from 'components/landing/Home';

const changeRouteHandle = (nextState, replace, callback) => {
	const pathname = nextState.location.pathname;
	const path = pathname.replace(URL_PREFIX, '');
	if(pathname === URL_PREFIX + 'login'){
		// Neu da login va vao trang login thi ve trang chu
		if(Tools.getToken()){
			replace(URL_PREFIX);
		}
	}else{
		// Neu khac trang login thi kiem tra login
		const login = Tools.checkLoginRequiredRoute(nextState.routes, path);
		if(login && !Tools.getToken()){
			replace(URL_PREFIX + 'login');
		}
	}
	callback();
}

const onEnter = (nextState, replace, callback) => {
	changeRouteHandle(nextState, replace, callback);
}

const onChange = (prevState, nextState, replace, callback) => {
	changeRouteHandle(nextState, replace, callback);
}
const rootElementAdmin = (
	<Provider store={store}>
		<div>
			<Router history={history}>
				<Route path={URL_PREFIX} component={App} onChange={onChange} onEnter={onEnter}>
					{/* MAIN ROUTES */}
					<IndexRoute
						component={Profile}
						params={{login: true}}></IndexRoute>
					<Route
						path="login"
						component={Login}
						params={{login: false}}></Route>
					<Route
						path="reset_password_confirm/:type/:token"
						component={PasswordConfirm}
						params={{login: false}}></Route>
					<Route
						path="change_password_confirm/:type/:token"
						component={PasswordConfirm}
						params={{login: false}}></Route>

					{/* ADMIN ROUTES */}
					<Route
						path="admin"
						component={Admin}
						params={{login: true}}></Route>

					{/* USER ROUTES */}
					<Route
						path="user"
						component={User}
						params={{login: true}}></Route>

					{/* CONFIG ROUTES */}
					<Route
						path="config"
						component={Config}
						params={{login: true}}></Route>


					{/* MISSING ROUTES */}
					<Route
						path='*'
						component={NotFound}
						params={{login: false}}/>
				</Route>
			</Router>
			<ReduxToastr
				timeOut={4000}
				newestOnTop={false}
				position="bottom-right"/>
		</div>
	</Provider>
);

const rootElementUser = (
	<Provider store={store}>
		<div>
			<Router history={history}>
				<Route path={URL_PREFIX} component={App} onChange={onChange} onEnter={onEnter}>
					{/* MAIN ROUTES */}
					<IndexRoute
						component={Profile}
						params={{login: true}}></IndexRoute>
					<Route
						path="login"
						component={Login}
						params={{login: false}}></Route>
					<Route
						path="reset_password_confirm/:type/:token"
						component={PasswordConfirm}
						params={{login: false}}></Route>
					<Route
						path="change_password_confirm/:type/:token"
						component={PasswordConfirm}
						params={{login: false}}></Route>

					{/* MISSING ROUTES */}
					<Route
						path='*'
						component={NotFound}
						params={{login: false}}/>

				</Route>
			</Router>
			<ReduxToastr
				timeOut={4000}
				newestOnTop={false}
				position="bottom-right"/>
		</div>
	</Provider>
);

const rootElementLanding = (
	<Provider store={store}>
		<div>
			<Router history={history}>
				<Route path={URL_PREFIX} component={App} onChange={onChange} onEnter={onEnter}>
					{/* MAIN ROUTES */}
					<IndexRoute
						component={Home}
						params={{login: false}}></IndexRoute>

					{/* MISSING ROUTES */}
					<Route
						path='*'
						component={NotFound}
						params={{login: false}}/>
				</Route>
			</Router>
			<ReduxToastr
				timeOut={4000}
				newestOnTop={false}
				position="bottom-right"/>
		</div>
	</Provider>
);

if(['admin', 'radmin'].indexOf(APP) !== -1){
	ReactDOM.render(rootElementAdmin, document.getElementById('app'));
}else if(APP === 'user'){
	ReactDOM.render(rootElementUser, document.getElementById('app'));
}else{
	ReactDOM.render(rootElementLanding, document.getElementById('app'));
}
