import configSaga from 'components/config/sagas/sagas';
import { select, take } from 'redux-saga/effects';

function* watchAndLog() {
	while (true) {
		let action = yield take('*');
		const state = yield select();
		action.state = state;
		console.log('%c action', 'color: #103FFB; font-weight: bold', action);
	}
}

export default function* rootSaga() {
	yield [
		configSaga(),
		watchAndLog()
	]
}