import React from 'react';
import './styles.styl';


class ListSide extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
	    };
	}

	render() {
		return (
			<div>
				<br/>
				<div className="panel panel-primary">
					<div className="panel-heading">
						<h3 className="panel-title">{this.props.title}</h3>
					</div>
					<div className="panel-body" style={{padding: 0}}>

						<div className="list-group list-side">
							<a href="#" className="list-group-item">Cras justo odio</a>
							<a href="#" className="list-group-item">Dapibus ac facilisis in</a>
							<a href="#" className="list-group-item">Morbi leo risus</a>
							<a href="#" className="list-group-item">Porta ac consectetur ac</a>
							<a href="#" className="list-group-item">Vestibulum at eros</a>
						</div>

					</div>
				</div>
			</div>
		);
	}
}

ListSide.propTypes = {
	title: React.PropTypes.string.isRequired
};

ListSide.defaultProps = {
};

export default ListSide
