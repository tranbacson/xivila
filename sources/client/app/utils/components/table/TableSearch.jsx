import React from 'react';


export default class TableSearch extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			value: ''
	    };
	    this.handleChange = this.handleChange.bind(this);
	}
	handleChange(event){
		this.setState({value: event.target.value});
		this.props.onChange(event);
	}

	_renderCheckMark(){
		if(!this.props.bulkRemove){
			return null;
		}
		return (
			<span
				className="glyphicon glyphicon-ok"
				onClick={this.props.onCheckAll}>
			</span>
		);
	}

	render(){
		return (
			<tbody>
				<tr>
					<th>
						{this._renderCheckMark()}
					</th>
					<td colSpan={this.props.cols}>
						<input
							type="text"
							className="form-control"
							value={this.state.value}
							onChange={this.handleChange}
							placeholder="Enter keywords for searching..."/>
					</td>
				</tr>
			</tbody>
		)
	}
}

TableSearch.propTypes = {
	bulkRemove: React.PropTypes.bool
};

TableSearch.defaultProps = {
	bulkRemove: true
};
