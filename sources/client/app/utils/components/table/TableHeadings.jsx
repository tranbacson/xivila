import React from 'react';


export class TableHeadings extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
	    };
	}

	_renderRemoveButton(){
		if(this.props.showRemoveAllButton && this.props.bulkRemove){
			return(
				<span
					className="glyphicon glyphicon-remove"
					onClick={this.props.onRemoveAll}>
				</span>
			);
		}
		return null;
	}

	render(){
		var headings = this.props.headings.map(function(heading, i) {
			if(heading.heading){
				return(<TableHeading heading={heading.title} key={i}/>);
			}
		});
		return (
			<thead>
				<tr>
					<th className="left-tool">
						{this._renderRemoveButton()}
					</th>
					{headings}
					<th colSpan={2}>
						{this.props.children}
					</th>
				</tr>
			</thead>
		)
	}
}

TableHeadings.propTypes = {
	headings: React.PropTypes.array,
	bulkRemove: React.PropTypes.bool
};

TableHeadings.defaultProps = {
	headings: [],
	bulkRemove: true
};

export class TableHeading extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
	    };
	}

	render(){
		return (
			<th colSpan={this.props.cols}>{this.props.heading}</th>
		);
	}
}

TableHeadings.propTypes = {
	heading: React.PropTypes.string,
	cols: React.PropTypes.number,
	bulkRemove: React.PropTypes.bool
};

TableHeading.defaultProps = {
	heading: null,
	cols: 1,
	bulkRemove: true
};
