import React from 'react';


export class TableAddButton extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render(){
		return (
			<button
				onClick={this.props.onExecute}
				type="button"
				className="btn btn-success btn-block btn-xs">
		        <span className="glyphicon glyphicon-plus"></span>&nbsp;
				{this.props.title}
			</button>
		)
	}
}
TableAddButton.propTypes = {
	title: React.PropTypes.string,
	onExecute: React.PropTypes.func.isRequired
};
TableAddButton.defaultProps = {
	title: 'Add'
};


export class TableFilter extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			keyword: ''
		};
	    this.onFilter = this.onFilter.bind(this);
		this._renderBulkRemoveButton = this._renderBulkRemoveButton.bind(this);
	}

	onFilter(event){
		this.setState({keyword: event.target.value});
		this.props.onFilter(event);
	}

	_renderBulkRemoveButton(){
		if(this.props.bulkRemove){
			return(
				<span
					className="glyphicon glyphicon-remove"
					onClick={() => this.props.onRemove()}></span>
			);
		}
		return null;
	}

	render(){
		return (
			<div className="input-group">
				<span className="input-group-addon bulk-remove">
					{this._renderBulkRemoveButton()}
				</span>
				<input
					type="text"
					className="form-control"
					value={this.state.keyword}
					onChange={this.onFilter}
					placeholder="Enter keywords for searching..."/>
			</div>
		)
	}
}
TableFilter.propTypes = {
	onFilter: React.PropTypes.func.isRequired,
	onRemove: React.PropTypes.func.isRequired,
	bulkRemove: React.PropTypes.bool.isRequired
};
TableFilter.defaultProps = {
	bulkRemove: true
};


export class TableCheckAll extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render(){
		if(!this.props.bulkRemove){
			return null;
		}
		return (
			<span>
				<span
					className="glyphicon glyphicon-ok"
					onClick={this.props.onCheckAll}></span>
			</span>
		);
	}
}
TableCheckAll.propTypes = {
	bulkRemove: React.PropTypes.bool,
	onCheckAll: React.PropTypes.func.isRequired
};
TableCheckAll.defaultProps = {
	bulkRemove: true
};


export class TableRightTool extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
		this._renderRemove = this._renderRemove.bind(this);
		this._renderUpdate = this._renderUpdate.bind(this);
	}

	_renderRemove(){
		if(this.props.allowRemove){
			return (
				<span>
					&nbsp;&nbsp;&nbsp;
			        <span
			        	onClick={this.props.onRemove}
			        	className="glyphicon glyphicon-remove pointer"></span>
		        </span>
			);
		}
		return null;
	}

	_renderUpdate(){
		if(this.props.allowUpdate){
			return (
				<span
					onClick={this.props.toggleModal}
		        	className="glyphicon glyphicon-pencil pointer"></span>
			);
		}
		return null;
	}

	render(){
		return(
			<div className="center-align">
		        {this._renderUpdate()}
		        {this._renderRemove()}
			</div>
		)
	}
}
TableRightTool.propTypes = {
	toggleModal: React.PropTypes.func.isRequired,
	onRemove: React.PropTypes.func.isRequired,
	allowRemove: React.PropTypes.bool,
	allowUpdate: React.PropTypes.bool
};
TableRightTool.defaultProps = {
	allowRemove: true,
	allowUpdate: true
};


export class TableCheckBox extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render(){
		if(!this.props.bulkRemove){
			return (
				null
			);
		}
		return (
	        <input type="checkbox"
				checked={this.props.checked}
				onChange={this.props.onCheck}/>
		);
	}
}
TableCheckBox.propTypes = {
	checked: React.PropTypes.bool,
	bulkRemove: React.PropTypes.bool,
	onCheck: React.PropTypes.func.isRequired
};
TableCheckBox.defaultProps = {
	checked: false,
	bulkRemove: true
};