import React from 'react';
// import InfiniteScroll from 'redux-infinite-scroll';
import { FIELD_TYPE } from 'app/constants';
import Tools from 'helpers/Tools';


export class TableRows extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
	    };
	}

	render(){
		var rows = this.props.rows.map((row, i) => {
			return(
				<TableRow
					{...this.props}
					row={row}
					key={i}/>
				);
		});
		/*
		return (
			<InfiniteScroll
			holderType="tbody"
			items={rows}
			hasMore={this.props.hasMore}
			loadMore={this.props.loadMore}
			/>
		)
		*/
		return (
			<tbody>
				{rows}
			</tbody>
		);
	}
}

TableRows.propTypes = {
	rows: React.PropTypes.array
};

TableRows.defaultProps = {
	rows: []
};



export class TableRow extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
	    };
	    this._renderEdit = this._renderEdit.bind(this);
	}

	_renderEdit(row){
		if(this.props.editRule){
			const url = Tools.urlParamsProcessing(this.props.editRule, row);
			return(
				<a href={url}>
					<span className="glyphicon glyphicon-pencil pointer"></span>
				</a>
			);
		}
		return (
			<span
				onClick={() => this.props.onToggleEdit(row.id)}
				className="glyphicon glyphicon-pencil pointer"></span>
		);
	}

	_renderCheckbox(row){
		if(!this.props.bulkRemove){
			return null;
		}
		return (
			<input
				type="checkbox"
				checked={row.checked || false}
				onChange={(event) => this.props.onCheck(row.id, event.target.checked)}/>
		);
	}

	render(){
		const displayKeys = this.props.displayKeys;
		const row = this.props.row;
		let cells = Object.keys(displayKeys).map((key, i) => {
			let value = Tools.formatTableData(displayKeys, row, key);

			/* ADDITION PROCESSING */
			if(displayKeys[key].link){
				let urlParams = this.props.urlParams;
				value = (
					<a
						href={Tools.urlParamsProcessing(displayKeys[key].link, row, urlParams)}>
						{value}
					</a>
				);
			}

			return (
				<td key={i}>{value}</td>
			);
		});

		return (
			<tr>
				<td>
					{this._renderCheckbox(row)}
				</td>
				{cells}
				<td className="right-tool">
					{this._renderEdit(row)}
				</td>
				<td className="right-tool">
					<span
						onClick={() => this.props.onRemove(row.id)}
						className="glyphicon glyphicon-remove pointer"></span>
				</td>
			</tr>
		);
	}
}

TableRow.propTypes = {
	row: React.PropTypes.object,
	editRule: React.PropTypes.object,
	bulkRemove: React.PropTypes.bool
};

TableRow.defaultProps = {
	row: null,
	editRule: null,
	bulkRemove: true
};
