import React from 'react';
import Link from 'react-router/lib/Link';
import store from 'app/store';
import { intState } from 'app/store';
import {logout as logoutAction} from 'app/actions/actionCreators';

import Sidebar from 'react-sidebar';
import DropdownMenu from 'react-dd-menu';

import { APP_TITLE, URL_PREFIX, APP } from 'app/constants';
import Tools from 'helpers/Tools';
import {apiUrls} from 'components/auth/_data';

class NavWrapper extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			showHambuger: false,
			open: false,
			sidebarOpen: false,
			sidebarDocked: false,
			isMenuOpen: false
	    };
	    this.closeDropdown = this.closeDropdown.bind(this);
	    this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
	    this.mediaQueryChanged = this.mediaQueryChanged.bind(this);
	    this.toggleDropdown = this.toggleDropdown.bind(this);
	}

	componentDidMount(){
		const mql = window.matchMedia(`(min-width: 800px)`);
	    mql.addListener(this.mediaQueryChanged);

	    this.setState({mql: mql});
	    this.setState({sidebarDocked: mql.matches});
		this.setState({sidebarOpen: mql.matches});
	    this.setState({showHambuger: !mql.matches});
	}

	componentWillUnmount() {
		this.state.mql.removeListener(this.mediaQueryChanged);
	}

	mediaQueryChanged() {
		this.setState({sidebarDocked: this.state.mql.matches});
		this.setState({sidebarOpen: this.state.mql.matches});
	    this.setState({showHambuger: !this.state.mql.matches});
	}

	onSetSidebarOpen(open) {
		this.setState({sidebarOpen: open});
	}

	toggleDropdown(){
		this.setState({ isMenuOpen: !this.state.isMenuOpen });
	}

	closeDropdown(){
		this.setState({ isMenuOpen: false });
	}

	getFullName(){
		let fullName = '';
		if(this.props['data-user'].profile){
			fullName = `${this.props['data-user'].profile.first_name || ''} ${this.props['data-user'].profile.last_name || ''}`;
		}
		/*
		if(this.state.showHambuger){
			return null;
		}
		*/
		return fullName;
	}

	handleLogout(){
		Tools.apiCall(apiUrls.logout, {}, false).then((result) => {
			Tools.removeStorage('authData');
			Tools.goToUrl('login');
			setTimeout(()=>{
				location.reload();
			}, 100);
		});
	}

	_renderMenu(){

		if(['admin', 'radmin'].indexOf(APP) !== -1){
			return (
				<ul className="list-group">
					<Link to={URL_PREFIX} className="list-group-item" activeClassName="active" onlyActiveOnIndex>
						<span className="glyphicon glyphicon-home"/> &nbsp;
						Profile
					</Link>
					<Link to={Tools.toUrl('admin')} className="list-group-item" activeClassName="active">
						<span className="glyphicon glyphicon-user"/> &nbsp;
						Quản trị viên
					</Link>
					<Link to={Tools.toUrl('user')} className="list-group-item" activeClassName="active">
						<span className="glyphicon glyphicon-briefcase"/> &nbsp;
						Khách hàng
					</Link>


					<Link to={Tools.toUrl('config')} className="list-group-item" activeClassName="active">
						<span className="glyphicon glyphicon-cog"/> &nbsp;
						Cấu hình
					</Link>
				</ul>
			)
		}else{
			return (
				<ul className="list-group">
					<Link to={URL_PREFIX} className="list-group-item" activeClassName="active" onlyActiveOnIndex>
						<span className="glyphicon glyphicon-home"/> &nbsp;
						Profile
					</Link>
				</ul>
			)
		}
	}

	_renderToggleMenu(){
		if(this.state.showHambuger){
			return (
				<div className="heading non-printable">
					<span
						onClick={() => this.onSetSidebarOpen(true)}
						className="hambuger glyphicon glyphicon-menu-hamburger"></span>
					<span className="heading-logo">
						{APP_TITLE}
					</span>
					{this._renderRightDropdown()}
				</div>
			);
		}
		return (
			<div className="heading non-printable">
				{this._renderRightDropdown()}
			</div>
		);
	}

	_renderRightDropdown(){
		let toggleButton = <span type="button" className="pointer" onClick={this.toggleDropdown}>
			{this.getFullName()}&nbsp;
			<span className="caret"></span>
		</span>;
		let menuOptions = {
			isOpen: this.state.isMenuOpen,
			close: this.closeDropdown,
			toggle: toggleButton,
			animate: true,
			menuAlign: 'right',
			textAlign: 'left'
		};
		return (
			<span className="main-right-dropdown">
				<DropdownMenu {...menuOptions}>
					<li>
						<Link to={Tools.toUrl('')}>
							<span
								className="glyphicon glyphicon-user"></span>&nbsp; Profile
						</Link>
					</li>
					<li>
						<a onClick={() => this.handleLogout()}>
							<span
								className="glyphicon glyphicon-off"></span>&nbsp; Logout
						</a>
					</li>
				</DropdownMenu>
			</span>
		);
	}

	render(){
		let sidebarContent = (
			<div className="sidebar-content non-printable">
				<div className="heading">
					<span className="heading-logo">
						{APP_TITLE}
					</span>
				</div>
				{this._renderMenu()}
			</div>
		);
		var styles = {
			sidebar: {
				backgroundColor: 'white',
				zIndex: 4,
				transition: 'none',
    			WebkitTransition: 'none'
			},
			content:{
				transition: 'none',
    			WebkitTransition: 'none'
			},
			overlay: {
				zIndex: 3
			}
		};
		return (
			<Sidebar sidebar={sidebarContent}
				open={this.state.sidebarOpen}
				docked={this.state.sidebarDocked}
				onSetOpen={this.onSetSidebarOpen}
				className="main-sidebar"
				styles={styles}>
				<div className="content-wrapper">
					{ this._renderToggleMenu() }
					{ React.cloneElement(this.props.children, {...this.props}) }
				</div>
			</Sidebar>
		);
	}
}

export default NavWrapper;
