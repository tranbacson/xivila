export const DEFAULT_TIMEZONE = 'Asia/Saigon'; // Asia/Ho_Chi_Minh
export const TOKEN_LIFE = 1440;
export const PASSWORD_TOKEN_LIFE = 30;
export const SADMIN_ROLE = 'quan-tri-vien';
export const PAGE_SIZE = 10;
