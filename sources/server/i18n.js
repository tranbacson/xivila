let i18n = require('i18n');

i18n.configure({
    locales:['vi', 'en'],
    directory: __dirname + '/locales',
    defaultLocale: 'vi',
    autoReload: true,
    updateFiles: false
});

export const validate = {
	notEmpty: {
	    msg: i18n.__('validate_invalid_string_empty')
	},
	isEmail: {
        msg: i18n.__('validate_invalid_email')
    },
    len: (min, max) => ({
        args: [min, max],
        msg: i18n.__('validate_invalid_string_lenght', min, max)
    })
}

export default i18n;
