import path from 'path';
import ResTools from '~/server/app/utils/helpers/ResTools';
import LandingRoute from '~/server/app/modules/landing/Landing.route';
import UserRoute from '~/server/app/modules/user/User.route';
import AdminRoute from '~/server/app/modules/admin/Admin.route';
import ConfigRoute from '~/server/app/modules/config/Config.route';
const express = require('express');

var app = module.exports = express();

app.use(function(req, res, next) {
	req.version = req.originalUrl.split('/')[2];
	next();
});
app.use('/', LandingRoute);
app.use('/user', UserRoute);
app.use('/admin', AdminRoute);
app.use('/config', ConfigRoute);

app.use(function(err, req, res, next) {
	// res.locals.message = err.message;
	// res.locals.error = req.app.get('env') === 'development' ? err : {};
	console.log('\x1b[31m', '[-]', '\x1b[0m', '\x1b[31m',err,'\x1b[0m');  //cyan
	res.json(ResTools.err(err));
});
