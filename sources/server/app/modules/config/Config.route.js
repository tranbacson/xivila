const express = require('express');
const route = express.Router();

import CustomMiddleware from '^/middlewares/customMiddleWare';
import ctrl from './Config.ctrl';


route.get('/list', CustomMiddleware.tokenRequired, ctrl.list);
route.get('/obj', CustomMiddleware.tokenRequired, ctrl.obj);
route.post('/add', CustomMiddleware.tokenRequired, ctrl.addItem);
route.post('/edit', CustomMiddleware.tokenRequired, ctrl.editItem);
route.post('/remove', CustomMiddleware.tokenRequired, ctrl.removeItem);

export default route;
