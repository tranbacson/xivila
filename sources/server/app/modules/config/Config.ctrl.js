import pick from 'lodash/pick';
import Config from './Config';
import ResTools from '^/utils/helpers/ResTools';


export default class ConfigCtrl {
	static list(req, res, next){
		const params = pick(req.query, Config.fillable);
		const keyword = req.query.keyword;
		const pageParams = {
			page: typeof req.query.page === 'undefined' ? 1 : parseInt(req.query.page),
			uri: req.originalUrl
		};
		Config.list(params, keyword, pageParams).then(result => res.json(result));
	}

	static obj(req, res, next){
		const id = parseInt(req.query.id);
		Config.obj(id).then(result => res.json(result));
	}

	static addItem(req, res, next){
		Config.addItem(req.body).then(result => res.json(result));
	}

	static editItem(req, res, next){
		const id = parseInt(req.body.id);
		Config.editItem(id, req.body).then(result => res.json(result));
	}

	static removeItem(req, res, next){
		const id = req.body.id;
		Config.removeItem(id).then(result => res.json(result));
	}
}