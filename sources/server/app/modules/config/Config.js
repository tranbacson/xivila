import bcrypt from 'bcrypt-nodejs';
import keys from 'lodash/keys';
import forEach from 'lodash/forEach';
import ResTools from '^/utils/helpers/ResTools';
import Token from '^/modules/token/Token';
import i18n from '~/server/i18n';
import schemas from '~/server/schemas';
import { PAGE_SIZE } from '~/server/constants';


export default class Config {
	static rules = {
		uid: 'STRING',
		value: 'STRING'
	};
	static get fillable(){
		return keys(this.rules);
	}

	// static async list(params={}, page=1, uri=null){
	static async list(params={}, keyword=null, pageParams={}){
		if(keyword && keyword.length > 2){
			params['$or'] = [
				{uid: {$iLike: `%${keyword}%`}},
				{value: {$iLike: `%${keyword}%`}}
			]
		}
		let {page, uri} = pageParams;
		let queryParms = {
			where: params,
			order: 'id DESC'
		}
		if(page !== null){
			queryParms.limit = PAGE_SIZE;
			queryParms.offset = PAGE_SIZE * (page - 1);
		}
		const result = await schemas.Config.findAndCount(queryParms);
		let extra = [];
		return ResTools.lst(result, extra, pageParams)
	}

	static async obj(params=null){
		try{
			let result = null;

			switch(typeof params){
				case 'number':
					result = await schemas.Config.find({where: {id: params}});
				break;
				case 'string':
					result = null;
				break;
				case 'object':
					if(params === null){
						result = null;
					}else{
						if(typeof params.id !== 'undefined'){
							result = await schemas.Config.find({where: {id: parseInt(params.id)}});
						}else{
							result = await schemas.Config.find({where: params});
						}
					}
				break;
				default:
					result = null
			}
			if(!result){
				return ResTools.err(i18n.__('item_not_exist'));
			}
			return ResTools.obj(result);
		}catch(err){
			return ResTools.err(err);
		}
	}

	static async addItem(params){
		try{
			const checkDuplicate = await schemas.Config.find({where: {uid: params.uid}});
			if(checkDuplicate){
				return ResTools.err(i18n.__('duplicate_item'));
			}
			const result = await schemas.Config.create(params, {fields: this.fillable});
			return ResTools.obj(result, i18n.__('add_success'));
		}catch(err){
			return ResTools.err(err);
		}
	}

	static async editItem(id, params){
		try{
			const item = await schemas.Config.find({where: {id}});
			if(!item){
				return ResTools.err(i18n.__('item_not_exist'));
			}
			const checkDuplicate = await schemas.Config.find({where: {uid: params.uid}});
			if(checkDuplicate && checkDuplicate.id !== id){
				return ResTools.err(i18n.__('duplicate_item'));
			}
			const result = await item.update(params, {fields: this.fillable});
			return ResTools.obj(result, i18n.__('edit_success'));
		}catch(err){
			return ResTools.err(err);
		}
	}

	static async removeItem(id){
		try{
			const listId = id.split(',');
			for(let i in listId){
				const newId = parseInt(listId[i]);
				const item = await schemas.Config.find({where: {id: newId}});
				if(!item){
					return ResTools.err(i18n.__('item_not_exist'));
				}
				await item.destroy();
			}
			const result = {
				id: listId
			}
			return ResTools.obj(result, i18n.__('remove_success'));
		}catch(err){
			return ResTools.err(err);
		}
	}
}