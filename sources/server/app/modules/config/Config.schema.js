"use strict";
import {validate} from '~/server/i18n';


module.exports = (sequelize, DataTypes) => {
    const {INTEGER, FLOAT, BOOLEAN, STRING, TEXT, DATE, DATEONLY} = DataTypes;
    const {notEmpty, len} = validate;

    var schema = sequelize.define("configs", {
        id: {type: INTEGER, primaryKey: true},
        uid: {
            type: STRING,
            validate: {notEmpty, len: len(1, 256)}
        },
        value: {
            type: STRING,
            validate: {notEmpty, len: len(1, 256)}
        }
    }, {
        timestamps: false
    });
    return schema;
};