import schemas from '~/server/schemas';
import Tools from '^/utils/helpers/Tools';
import {TOKEN_LIFE} from '~/server/constants';


export default class Token {
	static async newToken(userId, role, fingerprint){
		try{
			const key = role.role_type_uid + '_id';
	        let params = {
	            role: role.uid,
	            role_type: role.role_type_uid,
	            fingerprint: fingerprint,
	            token: Tools.randomStr(36)
	        };
	        params[key] = userId;
	        let destroyCondition = {};
	        destroyCondition[key] = userId;
	        let item = null;

	        try{
				const transaction = await schemas.sequelize.transaction();
				await schemas.Token.destroy({where: {...destroyCondition}});
		        item = await schemas.Token.create(params);
		        transaction.commit();
	        }catch(err){
	        	item = null;
	        	Tools.errorLog(err);
				transaction.rollback();
	        }

	        if(item){
		        return item.token;
	        }
	        return null;
		}catch(err){
			Tools.errorLog(err);
			return null;
		}
	}

	static checkExpired(token){
		try{
			const currentDate = Tools.now();
	        const tokenCreatedAt = Tools.dateParse(token.created_at);
	        const tokenLife = currentDate.diff(tokenCreatedAt, 'minutes');
	        if(tokenLife > TOKEN_LIFE){
	            return true;
	        }
	        return false;
		}catch(errors){
			Tools.errorLog(errors);
	        return true;
		}
	}
}