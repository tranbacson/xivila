"use strict";


module.exports = (sequelize, DataTypes) => {
    const {INTEGER, FLOAT, BOOLEAN, STRING, TEXT, DATE, DATEONLY} = DataTypes;
    var schema = sequelize.define("tokens", {
        id: {type: INTEGER, primaryKey: true, autoIncrement: true},
        admin_id: INTEGER,
        fingerprint: STRING,
        role: STRING,
        role_type: STRING,
        token: STRING,
        user_id: INTEGER
    }, {
        updatedAt: false,
        getterMethods : {
            parent: function(){
                if(this.admin_id){
                    return this.admin;
                }else if(this.user_id){
                    return this.user;
                }
                return null;
            },
            parent_id: function(){
                if(this.admin_id){
                    return this.admin_id;
                }else if(this.user_id){
                    return this.user_id;
                }
                return null;
            }
        },
        classMethods: {
            associate: function(schemas) {
                schema.belongsTo(schemas.Admin, {
                    foreignKey: 'admin_id',
                    constraints: false,
                    as: 'admin'
                });
                schema.belongsTo(schemas.User, {
                    foreignKey: 'user_id',
                    constraints: false,
                    as: 'user'
                });
            }
        },
        scopes: {
            parent: (Admin, User) => ({
                include: [
                    {model: Admin, as: 'admin'},
                    {model: User, as: 'user'}
                ]
            })
        }
    });

    return schema;
};