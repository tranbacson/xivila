const express = require('express');
const LandingRoute = express.Router();

import Tools from '^/utils/helpers/Tools';

/* GET home page. */
LandingRoute.get('/', function(req, res, next) {
	res.render('index', { title: 'Express' });
});

LandingRoute.get('/hello', function(req, res, next) {
	res.render('index', { title: 'Hello Express' });
});

LandingRoute.get('/helloa', function(req, res, next) {
	res.render('index', { title: 'HelloA Express' });
});

export default LandingRoute;
