"use strict";


module.exports = (sequelize, DataTypes) => {
    const {INTEGER, FLOAT, BOOLEAN, STRING, TEXT, DATE, DATEONLY} = DataTypes;
    var schema = sequelize.define("users", {
        id: {type: INTEGER, primaryKey: true},
        activate: BOOLEAN,
        address: STRING,
        admin_id: INTEGER,
        area_code_id: INTEGER,
        avatar: STRING,
        change_password_token: STRING,
        change_password_token_created: DATE,
        change_password_token_tmp: STRING,
        company: STRING,
        complain_day: FLOAT,
        // created_at: DATE,
        deposit_factor: FLOAT,
        email: STRING,
        fingerprint: STRING,
        first_name: STRING,
        last_login: DATE,
        last_name: STRING,
        login_failed: INTEGER,
        order_fee_factor: FLOAT,
        password: STRING,
        permissions: TEXT,
        phone: STRING,
        rate: INTEGER,
        real_delivery_fee_unit: INTEGER,
        remember_token: STRING,
        reset_password_token: STRING,
        reset_password_token_created: DATE,
        reset_password_token_tmp: STRING,
        role_id: INTEGER,
        signup_token: STRING,
        signup_token_created: DATE,
        uid: STRING
        // updated_at: DATE
    }, {
        getterMethods : {
            fullname: function(){return this.first_name + ' ' + this.last_name},
            role_uid: function(){
                if(this.role_id && this.role){
                    return this.role.uid;
                }
                return null;
            }
        },
        classMethods: {
            associate: function(schemas) {
                schema.belongsTo(schemas.Role, {
                    foreignKey: 'role_id',
                    constraints: false,
                    as: 'role'
                });
            }
        },
        scopes: {
            role: Role => ({
                include: [{model: Role, as: 'role'}]
            })
        }
    });
    return schema;
};