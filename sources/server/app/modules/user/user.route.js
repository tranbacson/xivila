const express = require('express');
const UserRoute = express.Router();

import CustomMiddleware from '^/middlewares/customMiddleWare';
import UserCtrl from './user.ctrl';

function countNum(req, res, next){
	// const route = req.baseUrl + req.route.path.split(':')[0];
	// console.log(route);
	res.send('respond with a resource COUNT');
}

/* GET users listing. */
UserRoute.get('/', function(req, res, next) {
	res.send('respond with a resource');
});

UserRoute.get('/count/:num', CustomMiddleware.tokenRequired, UserCtrl.countNum);

export default UserRoute;
