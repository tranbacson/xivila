import Admin from './Admin';
import ResTools from '^/utils/helpers/ResTools';
import Tools from '^/utils/helpers/Tools';


export default class AdminCtrl {
	static authenticate(req, res, next){
		const {email, password} = req.body;
		const fingerprint = req.get('Fingerprint');
		Admin.authenticate(email, password, fingerprint).then(result => res.json(result));
	}

	static logout(req, res, next){
		const fingerprint = req.get('Fingerprint');
		Admin.logout(req.token, fingerprint).then(result => res.json(result));
	}

	static profile(req, res, next){
		// Tools.sendEmail();
		const result = {
			id: req.token.parent.id,
			email: req.token.parent.email,
			first_name: req.token.parent.first_name,
			last_name: req.token.parent.last_name,
			fullname: req.token.parent.fullname
		}
		res.json(ResTools.obj(result));
	}

	static updateProfile(req, res, next){
		Admin.updateProfile(req.token.parent_id, req.body).then(result => res.json(result));
	}

	static list(req, res, next){
		res.send('respond with a resource COUNT: ' + req.version);
	}
}