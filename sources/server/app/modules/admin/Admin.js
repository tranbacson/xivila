import bcrypt from 'bcrypt-nodejs';
import keys from 'lodash/keys';
import forEach from 'lodash/forEach';
import ResTools from '^/utils/helpers/ResTools';
import Token from '^/modules/token/Token';
import i18n from '~/server/i18n';
import schemas from '~/server/schemas';


export default class Admin {
	static rules = {
		first_name: 'STRING,required',
		last_name: 'STRING'
	};
	static get fillable(){
		return keys(this.rules);
	}
	static async authenticate(email, password, fingerprint){
		try{
			const item = await schemas.Admin.scope({method: ['role', schemas.Role]}).find({where: {email}});
			if(item){
				if(bcrypt.compareSync(password, item.password)){
					const token = await Token.newToken(item.id, item.role, fingerprint);
					return ResTools.obj({
						id: item.id,
						first_name: item.first_name,
						last_name: item.last_name,
						fullname: item.fullname,
						email: item.email,
						role: item.role_uid,
						token: token
					}, i18n.__('authenticate_success'));
				}
			}
			return ResTools.err(i18n.__('wrong_email_password'));
		}catch(err){
			return ResTools.err(err);
		}
	}

	static async logout(token, fingerprint){
		try{
			if(token.fingerprint === fingerprint){
				token.destroy();
				return ResTools.obj({}, i18n.__('auth.logout_success'));
			}
			return ResTools.err(
				i81n.__('auth.not_enough_permission'),
				ResTools.ERROR_CODES.UNAUTHORIZED
			);
		}catch(err){
			return ResTools.err(err);
		}
	}

	static async obj(params=null){
		try{
			let result = null;

			switch(typeof params){
				case 'number':
					result = await schemas.Admin.find({where: {id: params}});
				break;
				case 'string':
					result = null;
				break;
				case 'object':
					if(params === null){
						result = null;
					}else{
						if(typeof params.id !== 'undefined'){
							result = await schemas.Admin.find({where: {id: parseInt(params.id)}});
						}else{
							result = await schemas.Admin.find({where: params});
						}
					}
				break;
				default:
					result = null
			}
			if(!result){
				return ResTools.err(i18n.__('item_not_exist'));
			}
			return ResTools.obj(result);
		}catch(err){
			return ResTools.err(err);
		}
	}

	static async updateProfile(id, params){
		try{
			if(typeof params.email !== 'undefined'){
				delete params.email
			}
			const item = await schemas.Admin.find({where: {id}});
			if(item){
				const result = await item.update(params, {fields: this.fillable});
				return ResTools.obj({
					id: result.id,
					first_name: result.first_name,
					last_name: result.last_name,
					fullname: result.fullname,
					email: result.email,
					role: result.role_uid
				}, i18n.__('edit_success'));
			}
			return ResTools.err(i18n.__('item_not_exist'));
		}catch(err){
			console.log(err.name);
			if(err.name === 'SequelizeValidationError'){
				let validateError = {}
				forEach(err.errors, error => {
					validateError[error.path]	= error.message;
				})
				return ResTools.err(validateError);
			}
			return ResTools.err(err);
		}
	}

	static async editItem(id, params){
		try{
			if(typeof params.email !== 'undefined'){
				delete params.email
			}
			console.log(params);
			const item = await schemas.Admin.find({where: {id}});
			if(item){
				const result = await item.update(params, {fields: this.fillable});
				return ResTools.obj(result, i18n.__('edit_success'));
			}
			return ResTools.err(i18n.__('item_not_exist'));
		}catch(err){
			return ResTools.err(err);
		}
	}
}