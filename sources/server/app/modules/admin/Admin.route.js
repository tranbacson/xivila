const express = require('express');
const route = express.Router();

import CustomMiddleware from '^/middlewares/customMiddleWare';
import AdminCtrl from './Admin.ctrl';


route.get('/', CustomMiddleware.tokenRequired, AdminCtrl.list);
route.post('/authenticate', AdminCtrl.authenticate);
route.post('/logout', CustomMiddleware.tokenRequired, AdminCtrl.logout);
route.get('/profile', CustomMiddleware.tokenRequired, AdminCtrl.profile);
route.post('/update-profile', CustomMiddleware.tokenRequired, AdminCtrl.updateProfile);

export default route;
