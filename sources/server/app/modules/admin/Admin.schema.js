"use strict";
import {validate} from '~/server/i18n';


module.exports = (sequelize, DataTypes) => {
    const {INTEGER, FLOAT, BOOLEAN, STRING, TEXT, DATE, DATEONLY} = DataTypes;
    const {notEmpty, isEmail, len} = validate;

    var schema = sequelize.define("admins", {
        id: {type: INTEGER, primaryKey: true},
        change_password_token: STRING,
        change_password_token_created: DATE,
        change_password_token_tmp: STRING,
        email: {
            type: STRING ,
            validate: {isEmail, len: len(5, 150)}
        },
        fingerprint: STRING,
        first_name: {
            type: STRING,
            validate: {len: len(5, 150)}
        },
        last_name: {
            type: STRING,
            validate: {notEmpty, len: len(5, 150)}
        },
        last_login: DATE,
        login_failed: INTEGER,
        password: STRING,
        permissions: TEXT,
        remember_token: STRING,
        reset_password_token: STRING,
        reset_password_token_created: DATE,
        reset_password_token_tmp: STRING,
        role_id: INTEGER,
        signup_token: STRING,
        signup_token_created: DATE
    }, {
        getterMethods : {
            fullname: function(){return this.first_name + ' ' + this.last_name},
            role_uid: function(){
                if(this.role_id && this.role){
                    return this.role.uid;
                }
                return null;
            }
        },
        classMethods: {
            associate: function(schemas) {
                schema.belongsTo(schemas.Role, {
                    foreignKey: 'role_id',
                    constraints: false,
                    as: 'role'
                });
            }
        },
        scopes: {
            role: Role => ({
                include: [{model: Role, as: 'role'}]
            })
        }
    });
    return schema;
};