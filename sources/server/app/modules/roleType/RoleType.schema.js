"use strict";


module.exports = (sequelize, DataTypes) => {
    const {INTEGER, FLOAT, BOOLEAN, STRING, TEXT, DATE, DATEONLY} = DataTypes;
    var schema = sequelize.define("role_types", {
        id: {type: INTEGER, primaryKey: true},
        title: STRING,
        uid: STRING
    },{
    	classMethods: {
            associate: function(schemas) {
                schema.hasMany(schemas.Role, {
                    foreignKey: 'role_type_id',
                    constraints: false,
                    as: 'roles'
                });
            }
        }
    });
    return schema;
};