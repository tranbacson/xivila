import bcrypt from 'bcrypt-nodejs';
import ResTools from '^/utils/helpers/ResTools';
import schemas from '~/server/schemas';

export default class Admin {
	static async authenticate(email, password, fingerprint){
		try{
			const item = await schemas.Admin.findOne({
				where: {email}
			});
			if(item){
				if(bcrypt.compareSync(password, item.password)){
					return ResTools.obj({
						id: item.id,
						fullname: item.fullname,
						email: item.email
					}, 'Authenticate success');
				}
			}
			return ResTools.err('Wrong email or password');
		}catch(err){
			return ResTools.err(err);
		}
	}
}