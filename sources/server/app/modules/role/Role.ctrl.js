import Admin from './Admin';

export default class AdminCtrl {
	static authenticate(req, res, next){
		const {email, password} = req.body;
		const fingerprint = req.get('Fingerprint');
		Admin.authenticate(email, password, fingerprint).then(result => res.json(result));
	}

	static list(req, res, next){
		res.send('respond with a resource COUNT: ' + req.version);
	}
}