"use strict";


module.exports = (sequelize, DataTypes) => {
    const {INTEGER, FLOAT, BOOLEAN, STRING, TEXT, DATE, DATEONLY} = DataTypes;
    var schema = sequelize.define("roles", {
        id: {type: INTEGER, primaryKey: true},
        default_role: BOOLEAN,
        detail: TEXT,
        role_type_id: INTEGER,
        role_type_uid: STRING,
        title: STRING,
        uid: STRING
    }, {
        classMethods: {
            associate: function(schemas) {
                schema.belongsTo(schemas.RoleType, {
                    foreignKey: 'role_type_id',
                    constraints: false,
                    as: 'roleType'
                });
            }
        },
        scopes: {
            roleType: RoleType => ({
                include: [{model: RoleType, as: 'roleType'}]
            })
        }
    });
    return schema;
};