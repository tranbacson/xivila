const express = require('express');
const route = express.Router();

import CustomMiddleware from '^/middlewares/customMiddleWare';
import AdminCtrl from './Admin.ctrl';


route.get('/', CustomMiddleware.tokenRequired, AdminCtrl.list);
route.post('/authenticate', AdminCtrl.authenticate);

export default route;
