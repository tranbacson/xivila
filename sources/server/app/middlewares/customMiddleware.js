import ResTools from '^/utils/helpers/ResTools';
import Tools from '^/utils/helpers/Tools';
import Token from '^/modules/token/Token';
import {SADMIN_ROLE} from '~/server/constants';
import i18n from '~/server/i18n';
const schemas = require('~/server/schemas');


export default class CustomMiddleware {
	static tokenRequired(req, res, next){
		if(req.method === 'OPTIONS') return next();
		const authHeader = req.get('Authorization');
        const fingerprint = req.get('Fingerprint');
        if(authHeader){
        	const tokens = authHeader.split(' ');
        	if(tokens.length == 2 && ['Token', 'Bearer'].indexOf(tokens[0]) !== -1){
        		let token = tokens[1];
        		try{
					schemas.Token.scope({method: ['parent', schemas.Admin, schemas.User]}).find({where: {token, fingerprint}}).then(token => {
						if(token && fingerprint){
	                        if(token.parent.block_account){
				        		return res.json(ResTools.err(
				        			i18n.__('block_account'),
							        ResTools.ERROR_CODES.UNAUTHORIZED
				        		));
	                        }else{
								if(!Token.checkExpired(token)){
									const currentRoute = req.baseUrl + req.route.path.split(':')[0];
                                	const allowRoutes = token.parent.permissions.split(',');
                                	let allow = false;
	                                if(allowRoutes.indexOf(currentRoute) !== -1){
	                                    allow = true;
	                                }
                                	if(token.role_type === 'admin'){
                                        if(token.role === SADMIN_ROLE || allow){
											token.created_at = Tools.now();
	                                        token.save();
	                                        req.token = token;
	                                        return next();
                                        }else{
							        		return res.json(ResTools.err(
							        			i18n.__('not_enough_permission'),
							        			ResTools.ERROR_CODES.FORBIDEN
							        		));
                                        }
                                	}else if(token.role_type === 'user'){
                                        if(allow){
											token.created_at = Tools.now();
	                                        token.save();
	                                        req.token = token;
	                                        return next();
                                        }else{
							        		return res.json(ResTools.err(
							        			i18n.__('not_enough_permission'),
							        			ResTools.ERROR_CODES.FORBIDEN
							        		));
                                        }
                                	}else{
                                		token.destroy().then(()=>{
							        		return res.json(ResTools.err(
							        			i18n.__('session_expired'),
							        			ResTools.ERROR_CODES.UNAUTHORIZED
							        		));
										});
                                	}
								}else{
									token.destroy().then(()=>{
						        		return res.json(ResTools.err(
						        			i18n.__('session_expired'),
						        			ResTools.ERROR_CODES.UNAUTHORIZED
						        		));
									});
								}
	                        }
						}else{
			        		return res.json(ResTools.err(
			        			i18n.__('login_required'),
			        			ResTools.ERROR_CODES.UNAUTHORIZED
			        		));
						}
	                });
        		}catch(errors){
        			Tools.errorLog(errors);
	        		return res.json(ResTools.err(
	        			i18n.__('login_required'),
	        			ResTools.ERROR_CODES.UNAUTHORIZED
	        		));
        		}
        	}else{
        		return res.json(ResTools.err(
        			i18n.__('login_required'),
        			ResTools.ERROR_CODES.UNAUTHORIZED
        		));
        	}
        }else{
        	return res.json(ResTools.err(
        		i18n.__('login_required'),
        		ResTools.ERROR_CODES.UNAUTHORIZED
        	));
        }
	}
}

