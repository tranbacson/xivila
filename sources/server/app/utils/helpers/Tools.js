import forEach from 'lodash/forEach';
import trim from 'lodash/trim';
import camelCase from 'lodash/camelCase';
import upperFirst from 'lodash/upperFirst';
import kebabCase from 'lodash/kebabCase';
import nodemailer from 'nodemailer';
import moment from 'moment-timezone';
import {DEFAULT_TIMEZONE} from '~/server/constants';


export default class Tools {
	static errorLog(errors){
		console.log('\x1b[31m', '[-]', '\x1b[0m', '\x1b[31m',errors,'\x1b[0m');
	}

	static now(){
		return moment().tz(DEFAULT_TIMEZONE);
	}

	static dateParse(dateString){
		try{
			return moment(dateString).tz(DEFAULT_TIMEZONE);
		}catch(errors){
			return null;
		}
	}

	static listRouter(router){
		let listUrl = [];
		forEach(router.stack, item => {
			if(item.name === 'router'){
				const regexpRoute = item.regexp.toString();
				let baseUrl = '/' + regexpRoute.split('\\/')[1];
				if(baseUrl === '?(?='){
					baseUrl = '';
				}
				forEach(item.handle.stack, stack => {
					let routeData = {
						route: null,
						module: upperFirst(camelCase(trim(baseUrl?baseUrl:'landing', '/'))),
						label: null
					};
					forEach(stack.route.stack, subStack => {
						if(subStack.name === 'tokenRequired'){
							routeData.route = baseUrl + stack.route.path.split(':')[0];
						}else{
							routeData.label = upperFirst(kebabCase(subStack.name)).replace(/-/g, ' ');
						}
						if(routeData.route &&  routeData.label && routeData.label !== '<anonymous>'){
							listUrl.push(routeData);
						}
					});
				});
			}
		});
		return listUrl;
	}

	static sendEmail(){
		try{
			console.log('sending email...')
			let transporter = nodemailer.createTransport({
				host: process.env.MAIL_HOST,
			    port: process.env.MAIL_PORT,
			    secure: true,
			    auth: {
			        user: process.env.MAIL_USERNAME,
					pass: process.env.MAIL_PASSWORD
			    }
			});

			let mailOptions = {
				from: '"Trần Bắc Sơn" <'+process.env.MAIL_USERNAME+'>', // sender address
				to: 'tbson87@gmail.com, tbson87@yahoo.com', // list of receivers
				subject: 'Hello ✔', // Subject line
				text: 'Hello world ?', // plain text body
				html: '<b>Hello world ?</b>' // html body
			};

			transporter.sendMail(mailOptions, (error, info) => {
			    if (error) {
			        return Tools.errorLog(error);
			    }
			    console.log('Message %s sent: %s', info.messageId, info.response);
			});
		}catch(errors){
			this.errorLog(errors);
		}
	}

	static randomStr(length=16){
		let s = "";
		while(s.length<length&&length>0){
			let r = Math.random();
			s+= (r<0.1?Math.floor(r*100):String.fromCharCode(Math.floor(r*26) + (r>0.5?97:65)));
		}
		return s;
	}
}