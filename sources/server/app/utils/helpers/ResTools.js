import URI from 'urijs';
import { PAGE_SIZE } from '~/server/constants';


export default class ResTools {
	static ERROR_CODES = {
		OK: 200,
		BAD_REQUEST: 400,
		UNAUTHORIZED: 401,
		FORBIDEN: 403,
		NOT_FOUND: 404,
		METHOD_NOT_ALLOWED: 405,
		INTERNAL_SERVER_ERROR: 500
	}

	static async paginate(data, totalCount, page=1){
		return {
			page_count: await schema.count()
		}
	}
	static setPageUri(uri, page){
		if(!page || !uri){
			return null;
		}
		let newUri = new URI(uri);
		newUri.setSearch({page});
		return newUri.toString();
	}
	static lst(data, extra=[], pageParams={}){
		if(!data){
			data = [];
		}
		let {page, uri} = pageParams;

		let last_page = Math.ceil(data.count/PAGE_SIZE);
		let prevPage = null;
		let nextPage = null;
		if(page === null){
			last_page = 1;
		}
		if(page > 1){
			prevPage = page - 1;
		}
		if(page === last_page){
			nextPage = null
		}else{
			nextPage = page + 1;
		}
		let result = {
			success: true,
			status_code: 200,
			message: null,
			data: {
				items: data.rows,
				_meta: {
					current_page: page === null ? 1 : page,
					last_page,
					from: page === null ? 1 : (page - 1) * PAGE_SIZE + 1,
					to: page === null ? 1 : page * PAGE_SIZE,
					per_page: page === null ? data.rows.length : PAGE_SIZE,
					page_count: data.rows.length
				},
				_links: {
					next_link: this.setPageUri(uri, nextPage),
					last_link: this.setPageUri(uri, prevPage)
				}
			},
			extra
		}
		return result;
	}

	static obj(data, message=null, extra=[]){
		return {
			success: true,
			status_code: 200,
			message: message,
			extra: extra,
			data: data
		}
	}

	static err(errors, status_code=400){
		console.log('\x1b[31m', '[-]', '\x1b[0m', '\x1b[31m',errors,'\x1b[0m');  //cyan
		return {
			status_code,
			success: false,
			message: errors,
			data: null
		};
	}
}