import forEach from 'lodash/forEach';
import toString from 'lodash/toString';
import trim from 'lodash/trim';
import moment from 'moment-timezone';


export default class ValidateTools {
	static parseInputParams(params, rules){
		let result = {};
		forEach(rules, (rule, key) => {
			if(typeof params[key] !== 'undefined'){
				switch(rule){
					case 'STRING':
						result[key] = toString(params[key]);
					break;
					case 'INTEGER':
						result[key] = parseInt(params[key]);
					break;
					case 'FLOAT':
						result[key] = JSON.parse(params[key]);
					break;
					case 'BOOLEAN':
					break;
					case 'DATE':
					break;
					case 'DATETIME':
					break;
				}
			}
		});
	}
}