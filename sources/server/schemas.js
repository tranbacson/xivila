'use strict';

let fs = require('fs');
let path = require('path');
let Sequelize = require('sequelize');
let basename = path.basename(module.filename);
let env = process.env.NODE_ENV || 'development';
let config = require(__dirname + '/../../config/config.json')[env];
import upperFirst from 'lodash/upperFirst';


let sequelize = new Sequelize(config.database, config.username, config.password, config);
if (process.env.DATABASE_URL) {
    sequelize = new Sequelize(process.env.DATABASE_URL, config);
}

let db = {};

const modulePath = __dirname + '/app/modules';
fs.readdirSync(modulePath).forEach(module => {
	const filePath = path.join(modulePath, module, module + '.schema.js');
	if (fs.existsSync(filePath)) {
		let model = sequelize['import'](filePath);
    	db[upperFirst(module)] = model;
	}
});

Object.keys(db).forEach(function(modelName) {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
