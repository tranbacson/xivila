import LandingRoute from './app/modules/landing/Landing.route';
import UserRoute from './app/modules/user/User.route';
import AdminRoute from './app/modules/admin/Admin.route';
import ConfigRoute from './app/modules/config/Config.route';

module.exports = function(app){
	app.use('/', LandingRoute);
	app.use('/user', UserRoute);
	app.use('/admin', AdminRoute);
	app.use('/config', ConfigRoute);
	return app;
}
